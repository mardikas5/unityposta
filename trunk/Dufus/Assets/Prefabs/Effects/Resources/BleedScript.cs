﻿using UnityEngine;
using System.Collections;

public class BleedScript : MonoBehaviour {

    public float initialConeSize;
    public float initialParticles;
    public float initialVelocity;

    public float finalParticles;
    public float finalVelocity;
    public float fadeOutTime;

    public float initialParticleSize;
    public float finalParticleSize;
    /// <summary>
    /// the number of milliseconds between squirts
    /// </summary>
    public float squirtInterval;
    public float squirtLength;
    float squirtTimer;
    ParticleSystem pSys;
    float timer = 0;

    public GameObject burstEffect;
    public bool simulateSquirts = true;

	void Start () 
    {
        pSys = GetComponent<ParticleSystem>();
        pSys.startSpeed = initialVelocity;
        squirtTimer = squirtInterval / 1000;

        burstEffect = GameObject.Instantiate(burstEffect, transform.position, transform.rotation) as GameObject;
	}
	

	void FixedUpdate () 
    {
        timer += Time.fixedDeltaTime;
        
        float timeScale = 1 - ((fadeOutTime - timer) / fadeOutTime);       
        
        if (timer <= fadeOutTime)
        {
            pSys.emissionRate = Mathf.FloorToInt(initialParticles + ((finalParticles - initialParticles) * timeScale));
            pSys.startSpeed = initialVelocity + ((finalVelocity - initialVelocity) * timeScale);

            pSys.startSize = initialParticleSize + ((finalParticleSize - initialParticleSize) * timeScale);
        }
        else if (simulateSquirts)
        {
            if (burstEffect != null)
            {
                GameObject.Destroy(burstEffect);
            }
            squirtTimer -= Time.fixedDeltaTime;
            if (squirtTimer < squirtLength / 1000)
            {
                pSys.emissionRate = finalParticles * 2f;               
            }
            else
            {
                pSys.emissionRate = finalParticles * 0f;
            }
            if (squirtTimer < 0)
            {
                squirtTimer = squirtInterval / 1000;
            }
        }
	}
}
