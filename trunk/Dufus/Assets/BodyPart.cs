﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class BodyPart : MonoBehaviour
{
    public DmgModelHumanParts bodyPartObject;
    public List<WoundTypes> Wounds = new List<WoundTypes>();
    public GameObject MaskObj;
    
    public delegate void woundAddedHandler(object sender);
    public event woundAddedHandler newWound;
    public event woundAddedHandler removeWound;

    public void Start()
    {
            newWound += addDamageVisuals;
            removeWound += removeDamageVisuals;
    }
    public void treatWound(int index)
    {
        if (Wounds.Count > index)
        {
            Wounds.RemoveAt(index);
            removeWound(null);
        }
    }

    public void AddWound(WoundTypes wound)
    {
        try
        {
            if (newWound != null)
            {
                newWound(null);
            }
        }
        finally
        {
            Wounds.Add(wound);
        }
    }
    public void addDamageVisuals(object sender)
    {
        if (!MaskObj)
        {
            return;
        }
        int value = 0;
        foreach (WoundTypes wound in Wounds)
        {
            if (wound.Equals(WoundTypes.DeepWound))
            {                
                MaskObj.GetComponent<Image>().color = new Color(128, 0, 0, 128);
                return;
            }
            if (wound.Equals(WoundTypes.ShallowWound))
            {
                if (value > 2)
                {
                    value = 2;
                }
            }
            if (wound.Equals(WoundTypes.ShallowWound))
            {
                if (value > 1)
                {
                    value = 1;
                }
            }
        }
        if (value == 1)
        {
            MaskObj.GetComponent<Image>().color = new Color(48, 0, 0, 128);
            return;
        }
        if (value == 2)
        {
            MaskObj.GetComponent<Image>().color = new Color(16, 0, 0, 128);
            return;
        }
    }
    public void removeDamageVisuals(object sender)
    {
        if (MaskObj != null)
        {
            MaskObj.GetComponent<Image>().color = new Color(255, 255, 255, 128);
        }
    }
    public string statusToString()
    {
        string wounds = null;
        foreach (WoundTypes wt in Wounds)
        {
            if (wt == WoundTypes.DeepWound)
            {
                wounds += "A deep wound has been made to the bodypart" + "\n";
            }
            if (wt == WoundTypes.ShallowWound)
            {
                wounds += "A shallow wound has been made to the bodypart" + "\n";
            }
        }
        if (wounds != null)
        {
            return wounds;
        }
        else
        {
            return "No damage has been made to the bodypart";
        }
    }
}
