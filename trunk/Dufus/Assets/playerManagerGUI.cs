﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public class playerManagerGUI : MonoBehaviour
{
    public int MessageCount = 0;

    public List<GameObject> openContextMenus = new List<GameObject>();
    public List<GameObject> InventoryClosedItems = new List<GameObject>();
    public List<GameObject> AlwaysOpenItems = new List<GameObject>();

    public List<GameObject> DefaultItems = new List<GameObject>();
    public List<GameObject> dmgIcons = new List<GameObject>();
    public List<ListenBodyPart> listenBodyParts;

    GameObject bodyPartStatus;
    public GameObject player;
    public GameObject bodyPartStatusPos;

    public Text PlayerHealthText;
    public Text PlayerBloodText;

    public GameObject ChatBox;
    public GameObject ChatManager;
    public InputField ChatInputBox;
    public Text MessageArea;

    public EquipmentManager EquipmentManager;

    public delegate void messageListener(object sender);
    public event messageListener newMessage;
    GameObject IDManager;
    public void Start()
    {
        if (player != null)
        {
            IDManager = GameObject.Find("_IDManager");
            IDManager.GetComponent<IDManager>().equipmentManager = EquipmentManager;

            player.GetComponent<BaseAttributes>().bodyPartStatusManager.AddRange(GetComponentsInChildren<ListenBodyPart>());
            player.GetComponent<BaseAttributes>().setBodyPartListeners();
            player.GetComponent<PlayerGeneralInputs>().HealthText = PlayerHealthText;
            player.GetComponent<PlayerGeneralInputs>().BloodText = PlayerBloodText;
            listenBodyParts = player.GetComponent<BaseAttributes>().bodyPartStatusManager;
            AttachIconsToParts();

            player.GetComponent<BaseAttributes>().healthChanged += changeHealthText;
            player.GetComponent<BaseAttributes>().bloodChanged += changeBloodText;

            if (player.GetComponent<PlayerGeneralInputs>().chatBox == null)
            {
                player.GetComponent<PlayerGeneralInputs>().chatBox = ChatBox;
                player.GetComponent<PlayerGeneralInputs>().chatTypingScreen = ChatInputBox;
                player.GetComponent<PlayerGeneralInputs>().chatManager = player.gameObject;

                player.GetComponent<Comm>().messageArea = MessageArea;
            }

            newMessage += player.GetComponent<PlayerGeneralInputs>().newMessage;
            player.GetComponent<Inventory>().equipmentManager = EquipmentManager;
            
            CloseItems();
        }
    }
    void changeHealthText(object sender)
    {
        Debug.Log("newHealth");
        PlayerHealthText.text = player.GetComponent<BaseAttributes>().Health.ToString();
    }
    void changeBloodText(object sender)
    {
        PlayerBloodText.text = player.GetComponent<BaseAttributes>().currentBloodLevel.ToString("####.");
    }
    public void createStatusPanel(BodyPart listenPart, DmgModelHumanParts bodyPartObject, string statusString)
    {
        while (openContextMenus.Count > 0)
        {
            removeStatusPanel(0);
        }

        bodyPartStatus = Resources.Load("BodyPartStatus") as GameObject;
        bodyPartStatus = GameObject.Instantiate(bodyPartStatus);

        bodyPartStatus.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = bodyPartObject.ToString() + " Status: ";
        bodyPartStatus.transform.GetChild(0).GetChild(1).GetComponent<Text>().text = statusString;

        bodyPartStatus.transform.SetParent(transform);
        bodyPartStatus.transform.GetChild(0).GetComponent<RectTransform>().position = bodyPartStatusPos.transform.position;

        foreach (WoundTypes wound in listenPart.Wounds)
        {
            Rect tempRect = bodyPartStatus.transform.GetChild(0).GetComponent<RectTransform>().rect;
            bodyPartStatus.transform.GetChild(0).GetComponent<RectTransform>().rect.Set(tempRect.position.x, tempRect.position.y - 25, tempRect.width, tempRect.height + 50);
        }

        Button tempBut = bodyPartStatus.GetComponentInChildren<Button>();

        tempBut.onClick.RemoveAllListeners();
        tempBut.onClick.AddListener(() => { listenPart.treatWound(0); });

        openContextMenus.Add(bodyPartStatus);


    }
    public void removeStatusPanel(int index)
    {
        GameObject.Destroy(openContextMenus[index]);
        openContextMenus.RemoveAt(index);
    }
    public void ShowItems()
    {
        foreach (GameObject obj in DefaultItems)
        {
            obj.SetActive(true);
        }
    }
    public void CloseItems()
    {
        foreach (GameObject obj in InventoryClosedItems)
        {
            obj.SetActive(false);
        }
        if (openContextMenus.Count > 0)
        {
            for (int i = 0; i < openContextMenus.Count; )
            {
                removeStatusPanel(0);
            }
        }
    }
    public void AttachIconsToParts()
    {
        foreach (ListenBodyPart p in listenBodyParts)
        {
            foreach (GameObject icon in dmgIcons)
            {
                string tempName;
                string[] nameParts;

                nameParts = icon.name.Split('_');
                tempName = nameParts[1];

                if (tempName == p.name)
                {
                    //p.MaskObj = icon;
                    p.listenPart.MaskObj = icon;
                }
            }
        }
    }
    public void addMessageToChat(string s)
    {
        if (newMessage != null)
        {
            newMessage(null);
        }

        MessageArea.text += s;
        MessageCount++;
        MessageArea.GetComponent<RectTransform>().sizeDelta = new Vector2(265, MessageCount * MessageArea.GetComponent<Text>().fontSize);
    }
}
