﻿using UnityEngine;
using System.Collections;


public class AIScript : MonoBehaviour
{
    public float acceleration = 2f;
    public float maxRunSpeed = 5f;
    public float maxWalkSpeed = 2f;
    public float mouseSensitivity = 5f;

    public float resistance = 0.1f;
    public float currentMovement;
    public Vector3 targetPos;
    public Transform visionObj;

    private Vector3 moveVector = new Vector3(0, 0, 0);
    private bool falling = false;
    private bool sprinting = false;
    
    public AIState curAIState;
    public Factions faction;

    public Transform curTarget;

    // Use this for initialization
    void Start()
    {
        if (GetComponent<NavMeshAgent>())
        {
            NavMeshAgent tempNavAgent = GetComponent<NavMeshAgent>();
            tempNavAgent.acceleration = acceleration;
            tempNavAgent.speed = maxRunSpeed;
            tempNavAgent.autoBraking = true;
            tempNavAgent.angularSpeed = 360;
        }
        if (visionObj != null)
        {
            if (!visionObj.GetComponent<TriggerScript>())
            {
                visionObj.gameObject.AddComponent<TriggerScript>();
                visionObj.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
            }
            visionObj.GetComponent<TriggerScript>().triggerEnter += EvaluateObject;
        }
    }

    private void EvaluateObject(GameObject sender)
    {
        Debug.Log(sender.ToString());
        //Cast a ray to see if you can actually see the object.
        RaycastHit hit;
        sender = sender.GetComponentInParent<CharacterController>().gameObject;
        Debug.Log(sender.transform.position + ", " + transform.position);
        Debug.DrawLine(transform.position + transform.forward + new Vector3(0, GetComponent<CharacterController>().height, 0), (sender.transform.position + new Vector3(0, GetComponent<CharacterController>().height)), Color.yellow, 2f);

        if (Physics.Raycast(transform.position + transform.forward + new Vector3(0, GetComponent<CharacterController>().height - 0.5f, 0), (sender.transform.position + new Vector3(0, sender.GetComponent<CharacterController>().height - 0.5f, 0)) - (transform.position + transform.forward + new Vector3(0, sender.GetComponent<CharacterController>().height - 0.5f, 0)), out hit, 1000f))
        {
            if (hit.transform.GetComponentInParent<BaseAttributes>())
            {
                BaseAttributes tempObjAttributes = hit.transform.GetComponentInParent<BaseAttributes>();
                //if (tempObjAttributes.objFaction != faction)
                //{
                Debug.Log(tempObjAttributes.gameObject);
                curTarget = hit.transform.GetComponentInParent<PlayerController>().transform;
                //}
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    void FixedUpdate()
    {
        if (targetPos != null)
        {
            //moveTowards(targetPos);
        }
        if (curTarget != null)
        {
            curAIState = AIState.Combat;
            EngageObject(curTarget); 
        }
        EvaluateState();
    }
    /// <summary>
    /// Used to check whether the AI is going at the right speed/falling at etc.
    /// </summary>
    private void movementChecks()
    {
        if (moveVector.magnitude < acceleration * 0.8f * Time.fixedDeltaTime)
        {
            moveVector = new Vector3(0, moveVector.y, 0);
        }
        if (!falling)
        {
            moveVector = new Vector3(0, moveVector.y, 0) + (new Vector3(moveVector.x, 0, moveVector.z) * (1 - resistance));
            if (!sprinting)
            {
                if (((Mathf.Abs(moveVector.x) + Mathf.Abs(moveVector.z)) / Time.fixedDeltaTime) >= maxWalkSpeed)
                {
                    moveVector = new Vector3(0, moveVector.y, 0) + ((new Vector3(moveVector.x, 0, moveVector.z) * ((maxWalkSpeed * Time.fixedDeltaTime) / (Mathf.Abs(moveVector.x) + Mathf.Abs(moveVector.z)))));

                }
            }
            else
            {
                if (((Mathf.Abs(moveVector.x) + Mathf.Abs(moveVector.z)) / Time.fixedDeltaTime) >= maxRunSpeed)
                {
                    moveVector = new Vector3(0, moveVector.y, 0) + ((new Vector3(moveVector.x, 0, moveVector.z) * ((maxRunSpeed * Time.fixedDeltaTime) / (Mathf.Abs(moveVector.x) + Mathf.Abs(moveVector.z)))));

                }
            }
        }
        if (falling)
        {
            if (this.GetComponent<CharacterController>().velocity.y > -56) //terminal velocity for humans
            {
                moveVector -= new Vector3(0, 0.8f * Time.fixedDeltaTime, 0);
            }
            else
            {

            }
        }

        Vector3 direction = -transform.up;
        Ray ray = new Ray(transform.localPosition + (Vector3.up), direction);
        RaycastHit hit;
        Debug.DrawRay(ray.origin, ray.direction, Color.red, 1f);
        if (Physics.Raycast(ray, out hit, 5f))
        {
            //Debug.Log((hit.point - transform.position).magnitude);
            if ((Mathf.Abs(hit.point.y - transform.position.y)) > 0.3f)
            {
                falling = true;
            }
            else
            {
                transform.position += new Vector3(0f, 0.3f - Mathf.Abs(hit.point.y - transform.position.y), 0f);
                falling = false;
            }
        }
        else
        {
            falling = true;
        }
    }

    /// <summary>
    /// Moves the AI to the target vector3 position (via navmeshagent)
    /// </summary>
    /// <param name="position"></param>
    private void moveTowards(Vector3 position)
    {
        GetComponent<NavMeshAgent>().SetDestination(position);
    }
    /// <summary>
    /// Used to determine the state that the AI will set itself.
    /// </summary>
    private void EvaluateState()
    {

    }

    /// <summary>
    /// Used by the AI to look around and get an understanding of what is around it.
    /// </summary>
    private void EvaluateSurroundings()
    {
        
    }
    private void EngageObject(Transform attackObj)
    {
        if (GetComponent<BaseWeapon>().effectiveDistance > Mathf.Abs((attackObj.position - transform.position).magnitude))
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position + new Vector3(0, GetComponent<CharacterController>().height - 0.5f, 0), (attackObj.position + new Vector3(0, attackObj.GetComponent<CharacterController>().height - 0.5f, 0)) - (transform.position + new Vector3(0, attackObj.GetComponent<CharacterController>().height - 0.5f, 0)), out hit, 1000f))
            {
                if (hit.transform == attackObj.transform || hit.transform.GetComponentInParent<BaseAttributes>())
                {
                    FireWeapon(attackObj.position);
                }
                else
                {
                    //Go towards the player with some priority to cover.
                    moveTowards(attackObj.position);
                }
            }      
        }
        else
        {
            //Go towards the player with some priority to cover.
            moveTowards(attackObj.position);
        }
    }
    private void FireWeapon(Vector3 target)
    {
        Vector3 relativePos = target - transform.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        //Debug.Log(rotation);
        //transform.rotation = rotation;
        GetComponent<BaseWeapon>().CmdfireWeapon(rotation);
    }
}
