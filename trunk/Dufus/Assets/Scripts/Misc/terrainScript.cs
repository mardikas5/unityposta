﻿using UnityEngine;
using System.Collections;

public class terrainScript : MonoBehaviour
{
    public Terrain myTerrain;
    public float newHeight = 10;
    public bool update = false;

    int x;
    int y;
    int width;
    int height;
    int layerNum;


    //void Update()
    //{
    //    if (update)
    //    {
    //        DetailPrototype[] details = new DetailPrototype[terrain.terrainData.detailPrototypes.Length];
    //        for (int i = 0; i < terrain.terrainData.detailPrototypes.Length; ++i)
    //        {
    //            DetailPrototype t = terrain.terrainData.detailPrototypes[i];
    //            t.maxHeight = newHeight;
    //            details[i] = t;
    //        }
    //        terrain.terrainData.detailPrototypes = details;
    //        update = false;
    //        terrainStuff();
    //    }
    //}

    //void terrainStuff()
    //{
    //    int[,] detailLayer = null;
    //    int numDetails = terrain.terrainData.detailPrototypes.Length;
    //    int[, ,] detailMapData = new int[terrain.terrainData.detailWidth, terrain.terrainData.detailHeight, numDetails];

    //    for (int layerNum = 0; layerNum < numDetails; layerNum++)
    //    {
    //        detailLayer = terrain.terrainData.GetDetailLayer(x, y, width, height, layerNum);
    //    }

    //    for (int n = 0; n < detailMapData.Length; n++) 
    //    {
    //        terrain.terrainData.SetDetailLayer(0, 0, n, detailLayer);
    //    }
    //}

    int hmWidth;
    int hmHeight;

    int posXInTerrain;
    int posYInTerrain;

    public Transform myTransform;
    private float regrowthTimer;
    private const float REGROWTH_SPEED = .001f;//in seconds

    private const int DETAIL_RESOLUTION = 1028;//Recommended 128 to 512
    private const int DETAIL_PER_PATCH = 16;//Recommended 16

    int[,] myOldMap;


    float xTime = 0.5f;
    void Start()
    {

        hmWidth = myTerrain.terrainData.heightmapWidth;
        hmHeight = myTerrain.terrainData.heightmapHeight;
        //myTerrain.terrainData.SetDetailResolution(DETAIL_RESOLUTION, DETAIL_PER_PATCH);

        //int[,] myMap = myTerrain.terrainData.GetDetailLayer(0, 0, DETAIL_RESOLUTION, DETAIL_RESOLUTION, 0);
        //int[,] newMap = new int[DETAIL_RESOLUTION, DETAIL_RESOLUTION];
        myOldMap = myTerrain.terrainData.GetDetailLayer(0, 0, DETAIL_RESOLUTION, DETAIL_RESOLUTION, 0);
    }

    void Update()
    {
        

    }
    private void makePlayerPath()
    {
        Vector3 tempCoord = (myTransform.localPosition - myTerrain.gameObject.transform.position);
        Vector3 coord;
        coord.x = tempCoord.x / myTerrain.terrainData.size.x;
        coord.y = tempCoord.y / myTerrain.terrainData.size.y;
        coord.z = tempCoord.z / myTerrain.terrainData.size.z;

        // get the position of the terrain heightmap where this game object is
        posXInTerrain = (int)(coord.x * hmWidth);
        posYInTerrain = (int)(coord.z * hmHeight);

        int[,] myMap = myTerrain.terrainData.GetDetailLayer(0, 0, DETAIL_RESOLUTION, DETAIL_RESOLUTION, 5);
        int[,] myMap2 = myTerrain.terrainData.GetDetailLayer(0, 0, DETAIL_RESOLUTION, DETAIL_RESOLUTION, 0);

        int[,] newMap = new int[DETAIL_RESOLUTION, DETAIL_RESOLUTION];
        int[,] newMap2 = new int[DETAIL_RESOLUTION, DETAIL_RESOLUTION];

        newMap = myMap;
        newMap2 = myMap2;

        newMap[posYInTerrain, posXInTerrain] = 45;
        newMap2[posYInTerrain, posXInTerrain] = 0;

        myTerrain.terrainData.SetDetailLayer(0, 0, 0, newMap2);
        myTerrain.terrainData.SetDetailLayer(0, 0, 5, newMap);

        xTime -= Time.deltaTime;
        if (xTime < 0.5f)
        {
            //SpawnTree(myTransform.localPosition);
            xTime = 5f;
        }
        if (update)
        {
            revert();
            update = false;
        }
    }
    public void revert()
    {
        myTerrain.terrainData.SetDetailLayer(0, 0, 0, myOldMap);
    }
    public void SpawnTree(Vector3 position)
    {
        if (myTerrain.terrainData.treePrototypes.Length > 0)
        {
            TreeInstance newInstance = new TreeInstance();
            newInstance.position = WorldToTerrainPoint(position);
            newInstance.heightScale = 2f;
            newInstance.widthScale = 2f;
            newInstance.prototypeIndex = 0;
            newInstance.lightmapColor = Color.white;
            newInstance.color = Color.white;
            myTerrain.AddTreeInstance(newInstance);
            myTerrain.Flush();
        }
    }

    Vector3 WorldToTerrainPoint(Vector3 pos)
    {
        Vector3 tempCoord = (pos - myTerrain.gameObject.transform.position);
        Vector3 coord;
        coord.x = tempCoord.x / myTerrain.terrainData.size.x;
        coord.y = tempCoord.y / myTerrain.terrainData.size.y;
        coord.z = tempCoord.z / myTerrain.terrainData.size.z;

        posXInTerrain = (int)(coord.x * hmWidth);
        posYInTerrain = (int)(coord.z * hmHeight);

        Debug.Log(posXInTerrain + ", " + posYInTerrain);
        return coord;
    }

}
