﻿using UnityEngine;
using System.Collections;

public class WaterReflection : MonoBehaviour {

    public GameObject sun;

    private WaterReflection myReflection;

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	if (Input.GetKeyDown("k"))
    {
        updateReflectionPos();
    }
	}

    void updateReflectionPos()
    {
        GetComponent<MeshRenderer>().material.SetVector("_WorldLightDir", new Vector4(sun.transform.eulerAngles.x,0,0,0));
    }
}
