﻿using UnityEngine;
using System.Collections;

[System.Flags]
public enum ItemSlotTypes
{
    Head = 1 << 0,
    Neck = 1 << 1,
    Body = 1 << 2,
    Undershirt = 1 << 3,
    Pants = 1 << 4,
    Feet = 1 << 5,
    WeaponLeft = 1 << 6,
    WeaponRight = 1 << 7
};

public enum NatureObjectTypes
{
    Blueberry = 1 << 0,
    Raspberry = 1 << 1,
    Poisonberry = 1 << 2,
};

public enum Factions
{
    Rebels = 1 << 0,
    Military = 1 << 1,
    Independent = 1 << 2
};

public enum AIState
{
    Idle = 1 << 0,
    Alerted = 1 << 1,
    Combat = 1 << 2,
    Retreating = 1 << 3
};

public enum DmgModelHumanParts
{
    Hips,

    RightThigh,
    RightLowerLeg,
    RightFoot,

    LeftThigh,
    LeftLowerLeg,
    LeftFoot,

    Stomach,
    Chest,
    Neck,
    Head,

    LeftHand,
    LeftLowerArm,
    LeftUpperArm,

    RightHand,
    RightLowerArm,    
    RightUpperArm,
};
public enum WoundTypes
{
    DeepWound = 1 << 0,
    ShallowWound = 1 << 1,
    Scratch = 1 << 2,
};
[System.Flags]
public enum WoundStatus
{
    Treated,
    Untreated,
    Infecteded,
    Bleeding,

};

public enum cameraState
{
    FirstPerson = 1 << 0,
    ThirdPerson = 1 << 1,
};

