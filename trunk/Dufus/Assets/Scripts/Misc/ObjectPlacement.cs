﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPlacement : MonoBehaviour {

    public float maxTime = 20f;
    public float interval = 60f;
    private float currentTime;
    private float checkTime = 0;

    public List<GameObject> possibleObjects = new List<GameObject>();

	void Start () 
    {

	}
	
	void Update () 
    {
        currentTime = Time.time;
        if (currentTime < maxTime)
        {
            if (checkTime > 0)
            {
                int check = Random.Range(0, 20);
                if (check > 18)
                {
                    generateObj(possibleObjects[0]);
                }
            }
        }
        else
        {
            generateObj(possibleObjects[0]);          
        }
	}
    public void generateObj(GameObject obj)
    {
        if (Network.isServer)
        {
            float x; float y; float z;

            x = Random.Range(-45, 45);
            y = 0.5f;
            z = Random.Range(-45, 45);

            obj = Network.Instantiate(possibleObjects[0], Vector3.zero, new Quaternion(0f, 0f, 0f, 0f), 2) as GameObject;
            obj.transform.position = new Vector3(x, y, z);
            maxTime = Time.time + interval;
            Debug.Log("objmade");
        }
    }
}
