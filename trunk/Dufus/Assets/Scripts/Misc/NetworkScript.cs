﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Threading;
using System.ComponentModel;
using System.Reflection;

public class NetworkScript : MonoBehaviour
{
    NetworkClient myClient;
    private const string typeName = "Pop";
    private const string gameName = "Blaster Boxes";
    private string PlayerName;
    
    public bool playerCreated = false;
    NetworkMessageDelegate myDeletegate;
    public InputField ipAddressField;

    private bool isRefreshingHostList = false;
    private HostData[] hostList;
    List<GameObject> tempServers = new List<GameObject>();

    public GameObject serverList;
    public GameObject button;

    public GameObject playerPrefab;
    public List<Transform> players = new List<Transform>();

    private List<string> debugList = new List<string>();
    private List<Text> infoList = new List<Text>();

    private GameObject curPlayer;
    public float debugTimer = 10;
    public int sizeX = 600; //propergui makeroni

    void Start()
    {
        debugList.Add("Initialized");
        serverList = GameObject.Find("ConnectionMenu");
        button = GameObject.Find("BaseButton");
        if (playerPrefab == null)
        {
            playerPrefab = Resources.Load("PlayerParent") as GameObject;
        }
    }

    void OnGUI()
    {
        if (PlayerName != null)
        {
            int lines = 0;

            for (int i = 0; i < debugList.Count; i++)
            {
                GUI.Label(new Rect(0, (Screen.height - 25) - 25 * i, 500, 50), debugList[(debugList.Count - 1) - i].ToString());
            }
            for (int i = 0; i < Network.connections.Length; i++)
            {
                GUI.Label(new Rect(0, 25 + lines * 25, 500, 500), Network.connections[i].ipAddress + " Ping: " + Network.GetLastPing(Network.connections[i]) + " GUID: " + Network.connections[i].guid);
                lines++;
            }
        }
    }

    public void StartServer()
    {
        
        GetComponent<NetworkManager>().StartHost();
        hideMenu();
    }

    void OnServerInitialized()
    {

    }

    void Update()
    {
        if (debugTimer < Time.time)
        {
            if (debugList.Count > 0)
            {
                debugList.RemoveAt(0);
            }
            debugTimer = Time.time + 5;
        }
    }

    public void RefreshHostList()
    {
        if (!isRefreshingHostList)
        {
            isRefreshingHostList = true;
            GameObject.Find("btnRefreshServers").GetComponent<Button>().interactable = false;
            MasterServer.RequestHostList(typeName);
        }

        debugList.Add("Refreshing list");
    }

    void OnMasterServerEvent(MasterServerEvent ServerEvent)
    {
        if (ServerEvent == MasterServerEvent.HostListReceived)
        {
            debugList.Add("Server list received!");
            GameObject.Find("btnRefreshServers").GetComponent<Button>().interactable = true;
            isRefreshingHostList = false;
            hostList = MasterServer.PollHostList();

            foreach (GameObject gameObj in tempServers)
            {
                GameObject.Destroy(gameObj);
            }
            foreach (Text textobj in infoList)
            {
                GameObject.Destroy(textobj);
            }
            createJoinButtons();
        }
    }

    public void JoinServer()
    {
        NetworkManager.singleton.networkAddress = ipAddressField.text;		
		NetworkManager.singleton.StartClient();
    }
    
    private void AddPlayer()
    {
        if (Network.isServer)
        {
            
        }
    }

    private void OnPlayerConnected(NetworkPlayer player)
    {
        debugList.Add("Player connected: " + player.ipAddress + player.guid);
        //GetComponent<NetworkView>().RPC("getPlayerList", RPCMode.Server);
    }

    public void btnDoneClick(InputField input)
    {
        PlayerName = input.text;
        createServerList();
    }

    public void createServerList()
    {
        GameObject tempObj = GameObject.Find("ConnectionMenu");
        tempObj.GetComponent<Canvas>().enabled = true;
        tempObj.GetComponent<StartMenuAnimations>().startAnim = true;
    }

    public void hideMenu()
    {
        GameObject tempObj = GameObject.Find("ConnectionMenu");
        tempObj.GetComponent<Canvas>().enabled = false;
        tempObj.GetComponent<CanvasGroup>().interactable = false;
        GameObject.Find("ChatBox").GetComponent<Canvas>().enabled = true;
    }

    void OnPlayerDisconnected(NetworkPlayer player)
    {
        if (Network.isServer)
        {
            Debug.Log(player.ToString());
            Debug.Log("Clean up after player " + player.ToString());
            Network.RemoveRPCs(player);
            Network.DestroyPlayerObjects(player);                     
        }

        playerCreated = false;
    }

    public void createJoinButtons()
    {
        if (hostList != null)
        {
            tempServers.Clear();
            int svrNr = tempServers.Count;

            foreach (HostData host in hostList)
            {
                GameObject tempButton = GameObject.Instantiate(button) as GameObject;
                GameObject tempInfo = GameObject.Instantiate(GameObject.Find("BaseText")) as GameObject;
                Button tempBut = tempButton.GetComponent<Button>();

                tempButton.name = "Join: " + host.gameName;

                tempButton.transform.SetParent(GameObject.Find("ServerScrollRect").transform);
                tempInfo.transform.SetParent(GameObject.Find("ServerScrollRect").transform);

                tempInfo.transform.position = tempButton.transform.position = tempButton.transform.parent.position;
                tempButton.transform.localPosition += new Vector3(-sizeX * 0, 200 - svrNr * 75, 0);
                
                tempInfo.transform.localScale = tempButton.transform.localScale = new Vector3(1, 1, 1);

                tempInfo.GetComponent<Text>().text = serverStats(host, tempInfo.GetComponent<Text>());
                
                StartCoroutine(pingServer(host, tempInfo.GetComponent<Text>()));
                tempInfo.transform.localPosition = tempButton.transform.localPosition + new Vector3(tempButton.GetComponent<RectTransform>().sizeDelta.x * 2, 0, 0);

                tempBut.onClick.RemoveAllListeners();
                //tempBut.onClick.AddListener(() => { JoinServer(host); hideMenu(); });

                tempButton.GetComponent<RectTransform>().sizeDelta = new Vector2(sizeX, 40);
                tempButton.GetComponentInChildren<Text>().text = " | " + host.gameName.ToString();
                tempButton.GetComponentInChildren<Text>().alignment = TextAnchor.LowerLeft;

                tempServers.Add(tempButton);
                infoList.Add(tempInfo.GetComponent<Text>());
                svrNr++;
            }
        }
    }
    public void exitGame()
    {
        Application.Quit();
    }

    public string serverStats(HostData host, Text textBox)
    {
        return " N/A " + " | " + host.connectedPlayers + " / " + host.playerLimit + " | ";
    }

    IEnumerator pingServer(HostData host, Text textBox)
    {
        float timer = Time.time + 10f;
        Ping servPing = new Ping(host.ip[0]);
        while (!servPing.isDone)
        {
            if (Time.time > timer)
            {
                yield break;
            }
            yield return new WaitForSeconds(1);
        }
        
        textBox.text = servPing.time + " | " + host.connectedPlayers + " / " + host.playerLimit + " | ";
    }
    //[RPC]
    public void newMessage()
    {
        curPlayer.GetComponent<PlayerGeneralInputs>().fadeChatCanvas(3f);
    }

    //[RPC]
    public void tellEveryoneNewPlayer(string name)
    {
        if (Network.isServer)
        {
            Debug.Log(name);
            curPlayer.GetComponentInChildren<HoverNames>().addToList(GameObject.Find(name), name);
            getPlayerList();
        }
    }
    /// <summary>
    /// Get the player list from the Server.
    /// </summary>
    //[RPC]    
    public void getPlayerList()
    {
        if (Network.isServer)
        {               
            foreach (Transform t in players)
            {
                if (t.name != "Player 0")
                {
                    //GetComponent<NetworkView>().RPC("setPlayerList", RPCMode.Others, t.name);
                    curPlayer.GetComponentInChildren<HoverNames>().addToList(t.gameObject, t.name);
                }
                else
                {
                    //GetComponent<NetworkView>().RPC("setPlayerList", RPCMode.Others, "player: " + PlayerName);
                }
            }
        }
    }

    //[RPC]
    public void setPlayerList(string name)
    {
            players.Add(GameObject.Find(name).transform);
            curPlayer.GetComponentInChildren<HoverNames>().addToList(GameObject.Find(name), name);
    }
    
}

