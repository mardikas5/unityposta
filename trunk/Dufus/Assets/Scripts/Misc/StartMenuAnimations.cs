﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
public class StartMenuAnimations : MonoBehaviour {

    public float StartPosX;
    public float StartPosY;
    public float smoothTime = 0.1f;
    public float timer;
    public bool startAnim = true;

    List<Transform> menuControls = new List<Transform>();
    List<Vector3> originalPositions = new List<Vector3>();
	void Start () 
    {
        timer = 5f;
        for (int i = 0; i < transform.childCount; i++)
        {
            menuControls.Add(transform.GetChild(i));
            originalPositions.Add(transform.GetChild(i).position);
        }
        foreach (Transform t in menuControls)
        {
            t.position += new Vector3(StartPosX, StartPosY);
        }
        GetComponent<Image>().color = Color.black;
	}


    void Update() 
    {
        if (startAnim)
        {
            for (int i = 0; i < menuControls.Count; i++)
            {
                if (Time.time > i * 0.75f)
                {
                    menuControls[i].position = Vector3.Lerp(menuControls[i].position, originalPositions[i], smoothTime);
                }
            }
            timer -= Time.deltaTime;
        }
        if (timer < 0)
        {
            startAnim = false;
        }
        GetComponent<Image>().color = Color.Lerp(GetComponent<Image>().color, Color.red, 0.01f);
	}
}
