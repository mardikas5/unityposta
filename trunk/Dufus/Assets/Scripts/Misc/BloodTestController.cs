﻿using UnityEngine;
using System.Collections;

public class BloodTestController : MonoBehaviour
{
	public void Update()
    {
        if( Input.GetMouseButtonDown( 0 ) )
        {
            Camera camera = Camera.main;

            Ray ray = camera.ViewportPointToRay( camera.ScreenToViewportPoint( Input.mousePosition ) );

            RaycastHit info;
            if( Physics.Raycast( ray, out info ) )
            {
                info.transform.root.SendMessage( "TakeHit", info, SendMessageOptions.DontRequireReceiver );
            }
        }
    }
}
