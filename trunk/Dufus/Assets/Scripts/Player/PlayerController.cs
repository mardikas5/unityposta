﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Internal;
using UnityEngine.Networking;


public class PlayerController : NetworkBehaviour
{
    public float acceleration = 0.5f;
    public float maxRunSpeed = 5f;
    public float maxWalkSpeed = 2f;
    public float mouseSensitivity = 5f;

    /// <summary>
    /// the percent of speed slowed per fixedframe.
    /// </summary>
    public float resistPercent = 0.1f;
    public float currentMovement;

    public float stepInterval = 0.5f;
    public Vector3 previousPos;
    public int layerMask = 1 << 0;

    private float jumpTimer = 0.5f;
    
    private AudioClip steppingSound;

    public bool falling = false;
    private bool sprinting = false;

    private Vector3 moveVector = new Vector3(0, 0, 0);

    private NetworkPlayer Netplayer;
    private CharacterController charController;

    public float timeScale = 1f;

    public GameObject playerWeapon;
    public GameObject AudioManager;
    public GameObject networking;
    public GameObject inventoryManager;

    public IDManager IDManager;
    
    void Awake()
    {
        previousPos = Vector3.zero;
    }

    void Start()
    {
        charController = gameObject.GetComponent<CharacterController>();
        IDManager = GameObject.Find("_IDManager").GetComponent<IDManager>();
        IDManager.playerController = this;

        if (isLocalPlayer)
        {
            networking = GameObject.Find("_Networking");
            steppingSound = Resources.Load("wawa") as AudioClip;
            AudioManager = GameObject.Find("_AudioManager");

            Netplayer = Network.player;

            playerWeapon = GameObject.Instantiate(Resources.Load("Bare_Hands"), Vector3.zero, Quaternion.identity) as GameObject;
            
            playerWeapon.GetComponent<Animator>().runtimeAnimatorController = Resources.Load("Bare_Hands_Controller") as RuntimeAnimatorController;
            GetComponent<BaseWeapon>().WeaponAnimator = playerWeapon.GetComponent<Animator>();
            GetComponent<BaseWeapon>().shotEvent += syncAnimations;
            GetComponent<BaseWeapon>().WeaponAnimator.GetBehaviour<fireAnimScript>().variableChanged += syncAnimations;
        }
    }
    public void setWeaponParent(Transform parent)
    {
        playerWeapon.transform.SetParent(parent);
    }
    public void setWeaponHandsPos(Vector3 LocalPos, Quaternion rot)
    {
        playerWeapon.transform.localPosition = LocalPos;
        playerWeapon.transform.rotation = rot;
    }
    void Update()
    {
        if (isLocalPlayer)
        {
            Time.timeScale = timeScale;
            if (Input.GetAxisRaw("Mouse Y") != 0 || Input.GetAxisRaw("Mouse X") != 0)
            {
                GetComponent<PlayerCamera>().mouseMoveCamera();
            }
        }
    }

    void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            movementInput();
            movementChecks();
           
            currentMovement = (Mathf.Abs(moveVector.x) + Mathf.Abs(moveVector.z)) / Time.fixedDeltaTime;

            Vector3 prevPos = transform.position;

            charController.Move(moveVector);
            movementSounds();
        }
        else
        {            
            currentMovement = Mathf.Lerp(currentMovement, (Mathf.Abs((transform.position - previousPos).magnitude) / Time.fixedDeltaTime), 0.1f);
            previousPos = transform.position;
        }
    }
    private void movementSounds()
    {

    }

    private void movementChecks()
    {
        if (moveVector.magnitude < acceleration * 0.8f * Time.fixedDeltaTime)
        {
            moveVector = new Vector3(0, moveVector.y, 0);
        }
        if (!falling)
        {
            moveVector = new Vector3(0, moveVector.y, 0) + (new Vector3(moveVector.x, 0, moveVector.z) * (1 - resistPercent));
            if (!sprinting)
            {
                if (((Mathf.Abs(moveVector.x) + Mathf.Abs(moveVector.z)) / Time.fixedDeltaTime) >= maxWalkSpeed)
                {
                    moveVector = new Vector3(0, moveVector.y, 0) + ((new Vector3(moveVector.x, 0, moveVector.z) * ((maxWalkSpeed * Time.fixedDeltaTime) / (Mathf.Abs(moveVector.x) + Mathf.Abs(moveVector.z)))));
                }
            }
            else
            {
                if (((Mathf.Abs(moveVector.x) + Mathf.Abs(moveVector.z)) / Time.fixedDeltaTime) >= maxRunSpeed)
                {
                    moveVector = new Vector3(0, moveVector.y, 0) + ((new Vector3(moveVector.x, 0, moveVector.z) * ((maxRunSpeed * Time.fixedDeltaTime) / (Mathf.Abs(moveVector.x) + Mathf.Abs(moveVector.z)))));
                }
            }

        }
        if (falling)
        {
            if (charController.velocity.y > -56) //terminal velocity for humans
            {
                moveVector -= new Vector3(0, 0.8f * Time.fixedDeltaTime, 0);
            }
        }

        Vector3 direction = -transform.up;
        Ray ray = new Ray(transform.localPosition + (Vector3.up), direction);
        RaycastHit hit;
        int tempMask = ~layerMask;
        Debug.DrawRay(ray.origin, ray.direction, Color.red, 1f);
        if (Physics.Raycast(ray, out hit, 5, tempMask))
        {
            //Debug.Log((hit.point - transform.position).magnitude);
            //Debug.Log(hit.transform);
            if ((Mathf.Abs(hit.point.y - transform.position.y)) > 0.3f)
            {
                //Debug.Log(hit.transform);
                falling = true;
            }
            else
            {
                transform.position += new Vector3(0f, 0.3f - Mathf.Abs(hit.point.y - transform.position.y), 0f);
                falling = false;
            }
        }
        else
        {
            falling = true;
        }
    }

    private void movementInput()
    {
        if (!falling)
        {
            if (!GetComponent<PlayerGeneralInputs>().isTyping)
            {
                if (Input.GetKey(KeyCode.W))
                {
                    moveVector += (charController.transform.forward * acceleration * Time.fixedDeltaTime);
                }
                if (Input.GetKey(KeyCode.S))
                {
                    moveVector += (-charController.transform.forward * acceleration * Time.fixedDeltaTime);
                }
                if (Input.GetKey(KeyCode.D))
                {
                    if (Input.GetKey(KeyCode.S) || (Input.GetKey(KeyCode.W)))
                    {
                        if (Input.GetKey(KeyCode.W))
                        {
                            moveVector -= (charController.transform.forward * acceleration * Time.fixedDeltaTime) * 0.5f;
                            moveVector += (charController.transform.right * acceleration * Time.fixedDeltaTime) * 0.5f;
                        }
                        if (Input.GetKey(KeyCode.S))
                        {
                            moveVector += (charController.transform.right * acceleration * Time.fixedDeltaTime) * 0.5f;
                            moveVector -= (-charController.transform.forward * acceleration * Time.fixedDeltaTime) * 0.5f;
                        }
                    }
                    else
                    {
                        moveVector += (charController.transform.right * acceleration * Time.fixedDeltaTime);
                    }
                }
                if (Input.GetKey(KeyCode.A))
                {
                    if (Input.GetKey(KeyCode.S) || (Input.GetKey(KeyCode.W)))
                    {
                        if (Input.GetKey(KeyCode.W))
                        {
                            moveVector -= (charController.transform.forward * acceleration * Time.fixedDeltaTime) * 0.5f;
                            moveVector += (-charController.transform.right * acceleration * Time.fixedDeltaTime) * 0.5f;
                        }
                        if (Input.GetKey(KeyCode.S))
                        {
                            moveVector += (-charController.transform.right * acceleration * Time.fixedDeltaTime) * 0.5f;
                            moveVector -= (-charController.transform.forward * acceleration * Time.fixedDeltaTime) * 0.5f;
                        }
                    }
                    else
                    {
                        moveVector += (-charController.transform.right * acceleration * Time.fixedDeltaTime);
                    }
                }
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    if (jumpTimer < Time.time)
                    {
                        Jump();
                        jumpTimer = Time.time + Time.fixedDeltaTime * 5f;
                    }
                }
            }
            if (Input.GetKey(KeyCode.LeftShift))
            {
                sprinting = true;
                acceleration = 0.5f;
            }
            else
            {
                acceleration = 0.2f;
                sprinting = false;
            }
        }

        if (isLocalPlayer)
        {
            if (this.transform.position.y < -100)
            {
                this.transform.position = new Vector3(0, 40, 0);
            }
        }

        if (Input.GetKey(KeyCode.LeftAlt))
        {
            GetComponent<PlayerCamera>().freeLook = true;
        }
        else
        {
            GetComponent<PlayerCamera>().freeLook = false;
        }
    }

    private void Jump()
    {
        if (Mathf.Abs(moveVector.y) > 0.2f)
        {
            moveVector = new Vector3(moveVector.x, 0, moveVector.z);
        }
        moveVector += new Vector3(0, 10f * Time.fixedDeltaTime, 0);
        AudioManager.GetComponent<AudioManager>().stopSound(steppingSound.name);
    }

    public void syncAnimations(object sender)
    {
        if (GetComponent<BaseWeapon>() != null)
        {
            if (GetComponent<BaseWeapon>().WeaponAnimator.GetBool("combatIdle") != GetComponent<Animator>().GetBool("combatIdle"))
            {
                GetComponent<Animator>().SetBool("combatIdle", GetComponent<BaseWeapon>().WeaponAnimator.GetBool("combatIdle"));
                //GetComponent<NetworkView>().RPC("setAnimation", RPCMode.All, "combatIdle", playerWeapon.GetComponent<BaseWeapon>().WeaponAnimator.GetBool("combatIdle").ToString());
            }
            if (GetComponent<BaseWeapon>().WeaponAnimator.GetInteger("fireAnim") != GetComponent<Animator>().GetInteger("fireAnim"))
            {
                GetComponent<Animator>().SetInteger("fireAnim", GetComponent<BaseWeapon>().WeaponAnimator.GetInteger("fireAnim"));
                //GetComponent<NetworkView>().RPC("setAnimation", RPCMode.All, "fireAnim", playerWeapon.GetComponent<BaseWeapon>().WeaponAnimator.GetInteger("fireAnim").ToString());
            }
            //GetComponent<NetworkView>().RPC("setAnimation", RPCMode.All, "wepType", playerWeapon.GetComponent<BaseWeapon>().wepType.ToString());
        }
    }
    private void setAnimation(string animName, string value)
    {
        int parseValue = -1;
        if (!int.TryParse(value, out parseValue))
        {
            GetComponent<Animator>().SetBool(animName, bool.Parse(value));
        }
        else
        {
            GetComponent<Animator>().SetInteger(animName, int.Parse(value));
        }
    }
}

