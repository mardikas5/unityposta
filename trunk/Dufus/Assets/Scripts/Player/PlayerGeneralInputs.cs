﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine.Networking;

public class PlayerGeneralInputs : NetworkBehaviour
{
    public IDManager IDManager;

    public InputField chatTypingScreen;

    public GameObject chatBox;   
    public GameObject chatManager;
    public GameObject networking;
    public GameObject PlayerHud;

    public CharacterController charController;

    public string latestMessage = "";

    private bool fadingChatBox = false;

    public bool weaponArmed = false;

    public bool isTyping = false;

    public Text HealthText;
    public Text BloodText;
 
    
    void Start()
    {
        if (isLocalPlayer)
        {
            PlayerHud = GameObject.Find("PlayerHud");
 
            IDManager = GameObject.Find("_IDManager").GetComponent<IDManager>();
            IDManager.playerGeneralInputs = this;
            networking = GetComponent<PlayerController>().networking;

            fadeChatCanvas(3f);
        }
    }

    void Update()
    {
        if (isLocalPlayer)
        {
            if (chatBox != null)
            {
                InputChat();
            }
            if (!isTyping)
            {
                 //&& !GetComponent<InventoryManager>().isInventoryOpen) || Input.GetKeyDown(KeyCode.I)
                InputGeneral();
            }

            if (GetComponent<PlayerCamera>().ObjInteractionRayCast() != null)
            {
                interactWithObject(GetComponent<PlayerCamera>().ObjInteractionRayCast());
            }
        }
    }

    void FixedUpdate()
    {

    }


    private void InputChat()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (chatBox.GetComponent<Canvas>().enabled == false)
            {
                StopCoroutine("gradualFadeChatBox");
                isTyping = true;
                fadingChatBox = false;
                chatBox.GetComponent<CanvasGroup>().alpha = 1;
                chatBox.GetComponent<Canvas>().enabled = true;
            }

            latestMessage = chatTypingScreen.text;
            if (latestMessage != null && latestMessage != "")
            {
                char[] chars = latestMessage.ToCharArray();
                string formattedChat = "";
                if (chars[0] != '/')
                {
                    foreach (char c in chars)
                    {
                        if (c == '\n')
                        {
                        }
                        else
                        {
                            formattedChat += c;
                        }
                    }

                    CmdSendMessage(formattedChat);
                    chatTypingScreen.text = "";
                    latestMessage = "";
                    fadeChatCanvas(3f);

                    isTyping = false;
                }
            }
            else
            {
                isTyping = true;
                chatTypingScreen.Select();
            }
        }

        if (chatTypingScreen.text != "" && chatTypingScreen.text != null)
        {
            fadingChatBox = false;
            chatBox.GetComponent<CanvasGroup>().alpha = 1;
            chatBox.GetComponent<Canvas>().enabled = true;
        }
        else if (!fadingChatBox && !chatTypingScreen.isFocused && !isTyping)
        {
            fadeChatCanvas(3f);
        }
    }

    public void CmdSendMessage(string message)
    {
        //chatManager = GameObject.Find("GameController(Clone)");
        chatManager.GetComponent<Comm>().cmdsaystuff(message);
    }
    private void InputGeneral()
    {
            if (Input.GetKeyDown(KeyCode.I))
            {                
                GetComponent<InventoryManager>().toggleOwnedInventories();
            }

            if (Input.GetKeyDown(KeyCode.C))
            {
                GetComponent<PlayerCamera>().cycleStates();
            }

            if (Input.GetMouseButton(0))
            {
                if (GetComponent<BaseWeapon>().WeaponArmed == true)
                {
                    GetComponent<BaseWeapon>().CmdfireWeapon(GetComponent<PlayerCamera>().camParent.transform.rotation);
                }
                else
                {
                   GetComponent<BaseWeapon>().changeWeaponArmState(true);
                }
            }
        

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            chatTypingScreen.enabled = false;
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            networking.GetComponent<NetworkScript>().getPlayerList();
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            GetComponent<BaseWeapon>().changeWeaponArmState(!weaponArmed);
        }

        if (Input.GetMouseButton(1))
        {
            
           GetComponent<BaseWeapon>().secondaryAction();
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            //selectInventoryItem(0);
        }

        if (Input.GetKeyDown(KeyCode.U))
        {
            //bk to hands 
            //if (playerWeapon.transform.parent != null)
            //{
            //    GameObject changeToWeapon = GameObject.Instantiate(Resources.Load("Bare_Hands"), new Vector3(0, 0, 0), camParent.transform.rotation) as GameObject;
            //    playerWeapon.GetComponent<BaseWeapon>().changeWeapon(camParent.transform.GetChild(0).gameObject, changeToWeapon);
            //    playerWeapon = changeToWeapon;
            //    GetComponent<PlayerController>().playerWeapon = changeToWeapon;
            //    playerWeapon.GetComponent<BaseWeapon>().owner = transform.GetComponent<NetworkTransform>().netId;
            //}
        }
    }

    public void showMenu()
    {
        GameObject.Find("MenuCanvas").GetComponent<Canvas>().enabled = true;
        GameObject.Find("ChatBox").GetComponent<Canvas>().enabled = false;
        GameObject.Find("ConnectionMenu").GetComponent<CanvasGroup>().interactable = true;
    }

    public void newMessage(object sender)
    {
        fadeChatCanvas(3f);
    }
    public void fadeChatCanvas(float timeToFade)
    {
        if (chatBox != null)
        {
            chatBox.GetComponent<Canvas>().enabled = true;
            chatBox.GetComponent<CanvasGroup>().alpha = 1;
            fadingChatBox = true;
            StopCoroutine("gradualFadeChatBox");
            StartCoroutine("gradualFadeChatBox",timeToFade);           
        }
    }

    IEnumerator gradualFadeChatBox(float time)
    {
        float timer = 2f;
        while (timer > 0)
        {
            timer -= Time.fixedDeltaTime;
            yield return new WaitForEndOfFrame();
        }
        while (chatBox.GetComponent<CanvasGroup>().alpha > 0 && fadingChatBox)
        {
            chatBox.GetComponent<CanvasGroup>().alpha -= Time.fixedDeltaTime / time;
            yield return new WaitForEndOfFrame();
        }
        if (fadingChatBox)
        {
            chatBox.GetComponent<Canvas>().enabled = false;
        }
    }



    void interactWithObject(GameObject gameObj)
    {
        if (gameObj == null)
        {
            return;
        }
        if (gameObj.tag == "Weapon")
        {
            if (!gameObj.transform.gameObject.GetComponent<outLine>())
            {
                gameObj.transform.gameObject.AddComponent<outLine>();
            }

            gameObj.transform.gameObject.GetComponent<outLine>().AddTime(Time.fixedDeltaTime);
            if (Input.GetKeyDown(KeyCode.E))
            {
                Inventory[] playerInventories = GetComponents<Inventory>();

                if (gameObj.GetComponent<Item>() && playerInventories.Length > 0)
                {
                    //playerInventories[0].addItem(gameObj);
                    for (int k = 0; k < playerInventories.Length; k++)
                    {
                        playerInventories[k].addItem(gameObj);
                        if (playerInventories[k].itemList.Contains(gameObj.GetComponent<Item>()))
                        {
                            k = playerInventories.Length;
                        }
                    }


                    if (!gameObj.GetComponent<Image>())
                    {
                        gameObj.AddComponent<Image>();
                        gameObj.GetComponent<RectTransform>().rotation = Quaternion.identity;
                        gameObj.GetComponent<Image>().sprite = Resources.Load<Sprite>(gameObj.GetComponent<Item>().imageName);
                    }
                    if (gameObj.GetComponent<Item>().placingInventory != null)
                    {
                        GameObject.Destroy(gameObj.GetComponent<Rigidbody>());
                    }
                }
                //refreshInventories();
            }
        }
    }




}
