﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour
{
    List<AudioSource> audios = new List<AudioSource>();
    public int nrOfAudioSources = 16;

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < nrOfAudioSources; i++)
        {
            GameObject tempObj = new GameObject();
            tempObj.AddComponent<AudioSource>();
            audios.Add(tempObj.GetComponent<AudioSource>());
            tempObj.name = "AudioLocation";
            tempObj.transform.parent = transform;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void playSound(string clipname, float minDistance, float maxDistance, float start, Vector3 pos)
    {
        for (int i = 0; i < audios.Count; i++)
        {
            if (!audios[i].isPlaying)
            {
                audios[i].transform.localPosition = pos;
                audios[i].clip = Resources.Load(clipname) as AudioClip;
                audios[i].time = start;
                audios[i].minDistance = minDistance;
                audios[i].maxDistance = maxDistance;
                audios[i].rolloffMode = AudioRolloffMode.Logarithmic;
                audios[i].Play();
                i = audios.Count + 1;
            }
        }
    }

    public void playSoundNoTime(string clipname, float minDistance, float maxDistance, Vector3 pos)
    {
		if (GetComponent<NetworkView>().isMine)
        {
            for (int i = 0; i < audios.Count; i++)
            {
                if (!audios[i].isPlaying)
                {
                    //Debug.Log(i);
                    audios[i].time = 0f;

                    audios[i].transform.localPosition = pos;
                    audios[i].clip = Resources.Load(clipname) as AudioClip;
                    audios[i].minDistance = minDistance;
                    audios[i].maxDistance = maxDistance;
                    audios[i].rolloffMode = AudioRolloffMode.Logarithmic;
                    audios[i].Play();
                    i = audios.Count + 1;
                }
            }
        }
    }

    public void stopSound(string clipname)
    {
        for (int i = 0; i < audios.Count; i++)
        {
            if (audios[i].isPlaying && audios[i].clip.name == clipname)
            {
                audios[i].Stop();
            }
        }
    }
}
