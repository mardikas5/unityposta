﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Comm : NetworkBehaviour
{
    public string playerName;
    public string message;
    public string latestMessage = "";

    public List<string> messages = new List<string>();

    public Canvas chatCanvas;
    public Text messageArea;
    public GameObject PlayerManager;



    void Start()
    {
        if (playerName == null)
        {
            playerName = "";
            chatCanvas.enabled = true;
        }
        PlayerManager = GameObject.Find("PlayerManager");
    }
    void displayMessages(object sender)
    {
        messageArea.text += messages[messages.Count - 1];
    }
    void Update()
    {

    }

    [Command]
    public void CmdsayStuff(string s)
    {
        RpcSay(s);
    }
    [ClientRpc]
    public void RpcSay(string s)
    {
        if (messageArea == null)
        {
            PlayerManager = GameObject.Find("PlayerManager");
            messageArea = GameObject.Find("PlayerManager").GetComponent<playerManagerGUI>().MessageArea;
        }
        Debug.Log("Message");
        message = "";
        message += GetComponent<NetworkIdentity>().netId + ": " + s + "\n";
        messages.Add(message);

        PlayerManager.GetComponent<playerManagerGUI>().addMessageToChat(message);
    }

    public void cmdsaystuff(string s)
    {
        Debug.Log("Message1");
        CmdsayStuff(s);
    }
}
