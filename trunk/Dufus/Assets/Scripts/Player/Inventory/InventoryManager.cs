﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Collections;

public enum playerMenus
{
    Inventory,
    Status
};

public class InventoryManager : NetworkBehaviour {

    public GameObject playerWeapon;
    public IDManager IDManager;
    public GameObject playerManagerScreen;
    public List<Inventory> playerInventories = new List<Inventory>();
    List<GameObject> displayingInventories = new List<GameObject>();
    List<Slot> tempSlots = new List<Slot>();
    public List<Item> representItems = new List<Item>();

    Inventory playerInventory;

    public bool isInventoryOpen = false;
	// Use this for initialization
    void Start()
    {
        if (isLocalPlayer)
        {
            IDManager = GameObject.Find("_IDManager").GetComponent<IDManager>();

            //playerManagerScreen.GetComponent<RectTransform>().position = new Vector3(0, 0, 0);
            //playerManagerScreen.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            //IDManager.GetComponent<IDManager>().equipmentManager = playerManagerScreen.GetComponentInChildren<EquipmentManager>();

            gameObject.AddComponent<Inventory>();
            playerInventory = gameObject.GetComponent<Inventory>();

            playerInventory.ownerTransform = transform;
            playerInventory.createSlots(20);

            playerInventories.Add(playerInventory);
            if (GameObject.Find("PlayerManager"))
            {
                GameObject.Destroy(GameObject.Find("PlayerManager"));
            }

            playerManagerScreen = Instantiate(Resources.Load("PlayerManager"), new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0)) as GameObject;
            playerManagerScreen.gameObject.name = "PlayerManager";

            playerManagerScreen.transform.SetParent(transform.parent);

            playerManagerScreen.GetComponent<playerManagerGUI>().player = gameObject;
        }

    }
	
	// Update is called once per frame
	void Update () 
    {
        //EquipmentBase.GetComponent<Canvas>().enabled = !EquipmentBase.GetComponent<Canvas>().enabled;
	}
    private void selectInventoryItem(int index)
    {
        if (playerInventory != null)
        {
            if (index < playerInventory.itemList.Count)
            {
                if (playerWeapon.transform.parent != null)
                {
                    GameObject changeToWeapon = GameObject.Instantiate(Resources.Load(playerInventory.itemList[index].itemName + "_Hands"), new Vector3(0, 0, 0), GetComponent<PlayerCamera>().camParent.transform.rotation) as GameObject;
                    playerWeapon.GetComponent<BaseWeapon>().changeWeapon(GetComponent<PlayerCamera>().camParent.transform.GetChild(0).gameObject, changeToWeapon);
                    playerWeapon = changeToWeapon;
                    GetComponent<PlayerController>().playerWeapon = changeToWeapon;
                    GetComponent<Animator>().SetInteger("wepType", playerWeapon.GetComponent<BaseWeapon>().wepType);
                    Debug.Log(playerWeapon.GetComponent<BaseWeapon>().wepType);
                    playerWeapon.GetComponent<BaseWeapon>().owner = GetComponent<NetworkTransform>().netId;
                }
            }
        }
    }
    public void toggleOwnedInventories()
    {
        if (displayingInventories.Count == 0)
        {
            playerManagerScreen.GetComponent<playerManagerGUI>().ShowItems();
            isInventoryOpen = true;
            foreach (Inventory i in playerInventories)
            {
                drawInventory(i, 10f);
            }
            foreach (Item I in representItems)
            {
                I.checkSlotPos = tempSlots;
            }
        }
        else
        {
            playerManagerScreen.GetComponent<playerManagerGUI>().CloseItems();
            isInventoryOpen = false;
            for (int i = displayingInventories.Count; i > 0; i--)
            {
                GameObject.Destroy(displayingInventories[0]);
                tempSlots.Clear();
                representItems.Clear();
                displayingInventories.RemoveAt(0);
            }
        }
    }

    public void addInventory(Inventory Inv)
    {
        playerInventories.Add(Inv);
        refreshInventories();
    }

    public void removeInventory(Inventory Inv)
    {
        if (playerInventories.Contains(Inv))
        {
            playerInventories.Remove(Inv);
            refreshInventories();
        }
    }

    /// <summary>
    /// Draws the inventory, with the amount of slots and draws all the items within that inventory.
    /// </summary>
    /// <param name="inventory"></param>
    /// <param name="slotPadding"></param>
    private void drawInventory(Inventory inventory, float slotPadding)
    {
        GameObject visualInventory = GameObject.Instantiate(Resources.Load("InventoryBase") as GameObject);

        displayingInventories.Add(visualInventory);
        visualInventory.transform.GetChild(0).GetComponent<Inventory>().ownerTransform = inventory.transform;
        if (inventory.savedPos != Vector3.zero)
        {
            visualInventory.transform.GetChild(0).transform.position = inventory.savedPos;
        }
        //getting inventories from items

        if (inventory.GetComponent<Item>())
        {
            visualInventory.transform.GetComponentInChildren<Text>().text = inventory.GetComponent<Item>().itemName + " Inventory";
        }
        else
        {
            visualInventory.transform.GetComponentInChildren<Text>().text = "Player Inventory";
        }

        //drawing inventory
        for (int i = 0; i < inventory.slotList.Count; i++)
        {
            Slot slotRepresent = Slot.Instantiate(inventory.slotList[i]);
            tempSlots.Add(slotRepresent);

            if (inventory.slotList[i].occupied)
            {
                slotRepresent.GetComponent<CanvasGroup>().alpha = 0.4f;
            }
            else
            {
                slotRepresent.GetComponent<CanvasGroup>().alpha = 0.8f;
            }

            slotRepresent.ownerInventory = inventory;
            slotRepresent.gameObject.AddComponent<Button>();
            slotRepresent.gameObject.AddComponent<Image>();
            slotRepresent.gameObject.GetComponent<Button>().image = slotRepresent.gameObject.GetComponent<Image>();
            slotRepresent.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(50f, 50f);

            slotRepresent.transform.SetParent(visualInventory.transform.GetChild(0).GetChild(1));
            slotRepresent.transform.localPosition = new Vector3(0, 0, 0);
            RectTransform slotBox = visualInventory.transform.GetChild(0).GetComponent<RectTransform>();

            if (i == 0)
            {
                slotRepresent.transform.localPosition += new Vector3(((-slotBox.sizeDelta.x / 2) + slotPadding * 5f), ((slotBox.sizeDelta.y / 2) - 100f), 0);
            }

            if (i % inventory.slotsInRow == 0 && i != 0)
            {
                slotRepresent.transform.localPosition += new Vector3(((-slotBox.sizeDelta.x / 2) + slotPadding * 5f), ((slotBox.sizeDelta.y / 2) - 100f), 0);
                slotRepresent.transform.localPosition += new Vector3(0, -((i / inventory.slotsInRow) * inventory.sizeY) - ((i / inventory.slotsInRow) * slotPadding), 0);
            }
            else if (i != 0)
            {
                slotRepresent.transform.localPosition += new Vector3(((-slotBox.sizeDelta.x / 2) + slotPadding * 5f) + (inventory.sizeX * (i % inventory.slotsInRow)) +
                ((i % inventory.slotsInRow) * slotPadding), -((i / inventory.slotsInRow) * inventory.sizeY) + ((slotBox.sizeDelta.y / 2) - 100f) - ((i / inventory.slotsInRow) * slotPadding), 0);
            }
            slotRepresent.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
        }

        //fill the new inventory with items.
        foreach (Item item in inventory.itemList)
        {
            GameObject represent = GameObject.Instantiate(item.gameObject) as GameObject;
            represent.GetComponent<Item>().onlyVisual = true;

            for (int i = 0; i < 10; i++)
            {
                if (represent.transform.childCount > 0)
                {
                    GameObject.Destroy(represent.transform.GetChild(0).gameObject);
                }
            }

            if (represent.transform.GetComponent<Inventory>())
            {
                GameObject.Destroy(represent.transform.GetComponent<Inventory>());
            }

            represent.GetComponent<Item>().currentInventory = inventory;
            represent.GetComponent<Item>().overWriteID(item.uniqueID);

            represent.transform.parent = visualInventory.transform.GetChild(0).GetChild(1).transform;
            represent.transform.localScale = new Vector3(1, 1, 1);
            represent.transform.rotation = Quaternion.identity;

            representItems.Add(represent.GetComponent<Item>());
        }
    }

    public void refreshInventories()
    {
        toggleOwnedInventories();
        toggleOwnedInventories();
    }
}
