﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class Item : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public int ID;
    public int sizeX;
    public int sizeY;
    public int inventorySpaces;
    public ItemSlotTypes ItemSlotType;

    public string itemName;
    public string description;
    public string imageName;
    public string packageName;
    public string uniqueID;

    public bool onlyVisual = false;
    public Guid GUID = Guid.NewGuid();

    private Vector3 startDragPos;
    /// <summary>
    /// The inventory that the item is currently in.
    /// </summary>
    public Inventory currentInventory;
    /// <summary>
    /// The inventory you want to place the item in.
    /// </summary>
    public Inventory placingInventory;
    /// <summary>
    /// The slots that the item will try to place itself in, whenever it is close enough
    /// </summary>
    public List<Slot> checkSlotPos = new List<Slot>();
    /// <summary>
    /// The slots the item has reservered and will reside in when the item is placed in the inventory.
    /// </summary>
    public List<int> reserveSlots = new List<int>();
    /// <summary>
    /// list used to visually check the item's position in an inventory
    /// </summary>
    public List<Slot> tempList = new List<Slot>();

    void Awake()
    {

    }

    void Start()
    {
        if (uniqueID.Length < 5)
        {
            uniqueID = GUID.ToString();
        }
        if (inventorySpaces > 0)
        {
            gameObject.AddComponent<Inventory>();
            gameObject.GetComponent<Inventory>().createSlots(inventorySpaces);
        }

        if (reserveSlots.Count > 0)
        {
            transform.localPosition = new Vector3(0, 0, 0);

            Vector3 offSet = new Vector3(0, 0, 0);

            for (int i = 0; i < reserveSlots.Count; i++)
            {
                for (int k = 0; k < reserveSlots.Count; k++)
                {
                    if (reserveSlots[i] == reserveSlots[k] && i != k)
                    {
                        reserveSlots.RemoveAt(i);
                    }
                }
            }

            //transform.parent = checkSlotPos[0].transform.parent;
            transform.localScale = new Vector3(1, 1, 1);

            Vector3 centered = Vector3.zero;
            for (int i = 0; i < reserveSlots.Count; i++)
            {
                placingInventory.slotList[reserveSlots[i]].occupied = true;
                if (checkSlotPos.Count > 0)
                {
                    centered += checkSlotPos[reserveSlots[i]].transform.localPosition;
                    checkSlotPos[reserveSlots[i]].GetComponent<CanvasGroup>().alpha = 0.4f;
                }
            }
            centered = centered / reserveSlots.Count;
            transform.localPosition = centered;

            for (int k = 0; k < checkSlotPos.Count; k++)
            {
                if (checkSlotPos[k].ownerInventory == placingInventory)
                {
                    tempList.Add(checkSlotPos[k]);
                    checkSlotPos[k].transform.GetComponentInParent<Canvas>().sortingOrder = 1;
                }
            }
        }
    }

    public void SetInitialPos(Inventory inventory)
    {
        placingInventory = inventory;
        Vector3 centered = new Vector3(0, 0, 0);
        for (int i = 0; i < reserveSlots.Count; i++)
        {
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            gameObject.GetComponent<Collider>().enabled = false;
            placingInventory.slotList[reserveSlots[i]].occupied = true;
            centered += placingInventory.slotList[reserveSlots[i]].transform.localPosition;
        }
        if (reserveSlots.Count == 0)
        {
            inventory.dropItem(this);
        }
        if (transform.childCount > 0)
        {
            transform.GetChild(0).localPosition = centered / reserveSlots.Count;
        }
        else
        {
            transform.localPosition = centered / reserveSlots.Count;
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        transform.parent = transform.parent.parent.parent;
        if (reserveSlots.Count > 0)
        {
            foreach (int i in reserveSlots)
            {
                placingInventory.slotList[i].occupied = false;
                tempList[i].occupied = false;
                tempList[i].GetComponent<CanvasGroup>().alpha = 0.8f;
            }
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        // check what inventory is wanted
        for (int i = 0; i < checkSlotPos.Count; i++)
        {
            if ((checkSlotPos[i].transform.position - (gameObject.transform.position - new Vector3(gameObject.transform.GetComponent<RectTransform>().sizeDelta.x * 0.25f - 5f, 0, 0))).magnitude < 50)
            {
                if (placingInventory != checkSlotPos[i].ownerInventory)
                {

                    placingInventory = checkSlotPos[i].ownerInventory;
                    tempList.Clear();

                    for (int k = 0; k < checkSlotPos.Count; k++)
                    {
                        if (checkSlotPos[k].ownerInventory == placingInventory)
                        {
                            tempList.Add(checkSlotPos[k]);
                            checkSlotPos[k].transform.GetComponentInParent<Canvas>().sortingOrder = 1;
                        }
                        else
                        {
                            checkSlotPos[k].transform.GetComponentInParent<Canvas>().sortingOrder = 2;
                        }
                    }
                }
            }
        }

        //do detection shit for that inventory
        if (checkSlotPos != null)
        {
            reserveSlots.Clear();
            transform.localPosition += new Vector3(eventData.delta.x, eventData.delta.y);
            for (int i = 0; i < placingInventory.slotList.Count; i++)   //number of slots in the wanted inventory
            {
                //get right slots from placement

                if ((tempList[i].transform.position - (gameObject.transform.position - new Vector3(gameObject.transform.GetComponent<RectTransform>().sizeDelta.x * 0.25f - 5f, 0, 0))).magnitude < 50)
                {
                    if (!tempList[i].occupied && placingInventory.ItemFits(this, i, false))
                    {
                        for (int ycheck = 0; ycheck < sizeY; ycheck++)
                        {
                            if (!reserveSlots.Contains(i + (ycheck * placingInventory.slotsInRow)))
                            {
                                reserveSlots.Add(i + (ycheck * placingInventory.slotsInRow));
                            }

                            for (int xcheck = 0; xcheck < sizeX; xcheck++)
                            {
                                if (!reserveSlots.Contains(i + ((placingInventory.slotsInRow * ycheck) + xcheck)))
                                {
                                    reserveSlots.Add(i + ((placingInventory.slotsInRow * ycheck) + xcheck));
                                }
                            }
                        }
                        i = checkSlotPos.Count;
                    }
                    else
                    {
                        reserveSlots.Clear();
                    }
                }
            }
            for (int i = 0; i < tempList.Count; i++)
            {
                if (!reserveSlots.Contains(i) && !tempList[i].occupied)
                {
                    tempList[i].GetComponent<CanvasGroup>().alpha = 0.8f;
                }
                else
                {
                    tempList[i].GetComponent<CanvasGroup>().alpha = 0.4f;
                }
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.parent = tempList[0].transform.parent;
        Vector3 centered = Vector3.zero;
        Debug.Log(GetComponent<Inventory>() + " " + placingInventory);
        if (reserveSlots.Count < 1)
        {
            for (int i = 0; i < currentInventory.itemList.Count; i++)
            {
                if (currentInventory.itemList[i].uniqueID == uniqueID)
                {
                    placingInventory.dropItem(currentInventory.itemList[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < currentInventory.itemList.Count; i++)
            {
                if (currentInventory.itemList[i].uniqueID == uniqueID)
                {
                    if (currentInventory.itemList[i].GetComponent<Inventory>() == placingInventory)
                    {
                        Debug.Log("placed in itself");
                        currentInventory.dropItem(currentInventory.itemList[i]);
                        return;
                    }
                    Debug.Log("found ID: " + i + " placing in: " + placingInventory.name + " removing from " + currentInventory.name);
                    placingInventory.itemList.Add(currentInventory.itemList[i]);
                    currentInventory.itemList[i].reserveSlots = reserveSlots;
                    currentInventory.itemList[i].placingInventory = placingInventory;
                    currentInventory.itemList[i].transform.parent = placingInventory.slotList[0].transform.parent;
                    currentInventory.itemList.RemoveAt(i);
                }
            }
            for (int i = 0; i < reserveSlots.Count; i++)
            {
                placingInventory.slotList[reserveSlots[i]].occupied = true;

                tempList[reserveSlots[i]].occupied = true;
                centered += tempList[reserveSlots[i]].transform.position;
            }
            transform.position = centered / reserveSlots.Count;
        }
    }

    public Vector3 getCenter()
    {
        Vector3 centered = Vector3.zero;
        for (int i = 0; i < reserveSlots.Count; i++)
        {
            //placingInventory.slotList[reserveSlots[i]].occupied = true;
            centered += checkSlotPos[reserveSlots[i]].transform.position;
        }
        return centered / reserveSlots.Count;
    }

    public void overWriteID(string newID)
    {
        uniqueID = newID;
    }

}
