﻿using UnityEngine;
using System.Collections;

public class Slot : MonoBehaviour {

    public Inventory ownerInventory;
    public bool occupied = false;
    public int itemID;

	void Start () 
    {
        if (ownerInventory == null)
        {
            GameObject.Destroy(transform.parent.gameObject);
        }
	}
	
	void Update () 
    {
	
	}

    void dropItem()
    {
        occupied = false;
        itemID = -1;
    }
}
