﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Item_ContextMenu : MonoBehaviour {

    public GameObject contextParent;

    private GameObject contextMenuText;
    public Transform parentObj = null;

    public bool displayContextMenu(Item item, Vector3 pos, Transform parent)
    {
        if (contextParent == null)
        {
            newInstance();
        }

        contextParent.GetComponent<RectTransform>().position = pos + new Vector3(contextParent.GetComponent<RectTransform>().sizeDelta.x/2,-contextParent.GetComponent<RectTransform>().sizeDelta.y/2,0);
        contextMenuText.GetComponent<Text>().text = item.description;
        contextMenuText.transform.localPosition = new Vector3(0, 0, 0);
        contextParent.transform.SetParent(parent);

        //generate equip

        Button equipBut = contextParent.transform.GetChild(0).GetComponent<Button>();
        equipBut.onClick.RemoveAllListeners();
        equipBut.onClick.AddListener(() => { item.placingInventory.equipItem(item); });
        

        //generate drop

        Button dropBut = contextParent.transform.GetChild(1).GetComponent<Button>();
        dropBut.onClick.RemoveAllListeners();
        dropBut.onClick.AddListener(() => { item.placingInventory.dropItem(item); });

        return true;
    }
    private void newInstance()
    {
        Debug.Log("newinstance");
        contextParent = GameObject.Instantiate(Resources.Load("ContextMenuBase") as GameObject);
        contextMenuText = new GameObject();
        contextMenuText.name = "ItemContextMenu";
        contextMenuText.AddComponent<RectTransform>();
        contextMenuText.AddComponent<Text>();
        contextMenuText.GetComponent<RectTransform>().sizeDelta = new Vector2(200f, 200f);
        contextMenuText.GetComponent<Text>().font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
        contextMenuText.GetComponent<Text>().text = "This is an item.";
        contextMenuText.transform.SetParent(contextParent.transform);
        contextMenuText.transform.position = new Vector3(0f, 0f, 0f);
    }

}

