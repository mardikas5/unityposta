﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;




public class Inventory : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public bool isDisplaying = false;
    public bool refreshRequired = false;
    public Vector3 savedPos = new Vector3(0, 0, 0);
    public IDManager IDManager;

    public List<Item> itemList = new List<Item>();
    public List<Slot> slotList = new List<Slot>();
    public List<GameObject> openContextMenus = new List<GameObject>();
    public Transform ownerTransform;
    public Text inventoryTitle;

    public GameObject equipmentScreen;
    public EquipmentManager equipmentManager;
    GameObject parentObject;

    public float paddingX = 15f;
    public float paddingY = 15f;
    public int slotsInRow = 5;

    public float sizeX = 50f;
    public float sizeY = 50f;

    float initialX;
    float initialY;

    float nextPosX;
    float nextPosY;

    float placeX = 0;
    float placeY = 0;

    void Awake()
    {
        foreach (Slot slot in slotList)
        {
            if (slot.occupied)
            {
                slot.GetComponent<CanvasGroup>().alpha = 0.4f;
            }
            else
            {
                slot.GetComponent<CanvasGroup>().alpha = 0.8f;
            }
        }
    }

    void Start()
    {
        try
        {
            IDManager = GameObject.Find("_IDManager").GetComponent<IDManager>();
            //equipmentManager = IDManager.equipmentManager;
            if (parentObject == null)
            {
                parentObject = new GameObject();
            }
        }
        catch
        {
        }
        finally
        {
            parentObject.name = transform.name + "_Inventory";
            parentObject.transform.parent = transform;

        }
    }

    public void createSlots(int nrOfSlots)
    {
        if (parentObject == null)
        {
            parentObject = new GameObject();
        }
        for (int i = 0; i < nrOfSlots; i++)
        {
            createNextSlot();
        }
    }

    public void createNextSlot()
    {
        int i = slotList.Count;
        createSlot(i);
    }

    void createSlot(float slotNr)
    {
        GameObject tempPanel = new GameObject();
        tempPanel.name = "slot" + slotNr;

        tempPanel.AddComponent<CanvasGroup>();
        tempPanel.transform.parent = parentObject.transform;
        tempPanel.AddComponent<Slot>();
        tempPanel.GetComponent<Slot>().ownerInventory = this;
        slotList.Add(tempPanel.GetComponent<Slot>());
    }

    public void addItem(GameObject item)
    {
        Debug.Log("Adding item: " + item);
        if (item.GetComponent<Item>())
        {
            if (item.GetComponent<Item>().GetComponent<Inventory>() == this)
            {               
                dropItem(item.GetComponent<Item>());
                return;
            }
            item.transform.rotation = Quaternion.identity;
            if (item.GetComponent<Item>().placingInventory != null)
            {
                item.GetComponent<Item>().placingInventory = gameObject.GetComponent<Inventory>();
            }
        }
        else
        {
            item = item.transform.GetChild(0).gameObject;
            item.GetComponent<Item>().placingInventory = gameObject.GetComponent<Inventory>();
        }

        itemList.Add(item.GetComponent<Item>());
        item.transform.SetParent(parentObject.transform);

        for (int i = 0; i < slotList.Count; i++)
        {
            if (i == slotList.Count - 1 && !ItemFits(item.GetComponent<Item>(), i, true))
            {
                dropItem(item.GetComponent<Item>());
            }
            if (ItemFits(item.GetComponent<Item>(), i, true))
            {
                i = slotList.Count;
                item.transform.localScale = new Vector3(1, 1, 1);
                item.transform.GetComponent<Rigidbody>().useGravity = false;
                item.transform.parent.localScale = new Vector3(1, 1, 1);
                item.transform.parent.localPosition = new Vector3(0, 0, 0);

                if (item.GetComponent<Inventory>())
                {
                    if (ownerTransform != null)
                    {
                        ownerTransform.GetComponent<InventoryManager>().addInventory(item.GetComponent<Inventory>());
                        ownerTransform.GetComponent<InventoryManager>().refreshInventories();
                    }
                }
            }
        }
    }

    public bool ItemFits(Item item, int slotNr, bool initialPlacement)
    {

        item.reserveSlots.Clear();

        if (!slotList[slotNr].occupied)
        {
            if (item.sizeY < 2)
            {
                if (item.sizeX < 2)
                {
                    if (initialPlacement)
                    {
                        item.SetInitialPos(this);
                    }
                    return true;
                }
            }
            if (slotList.Count > slotNr + (slotsInRow * (item.sizeY - 1)))
            {
                for (int ycheck = 0; ycheck < item.sizeY; ycheck++)
                {
                    if (slotList[slotNr + (slotsInRow * ycheck)].occupied)
                    {
                        if (initialPlacement)
                        {
                            item.reserveSlots.Clear();
                        }
                        return false;
                    }
                    else
                    {
                        item.reserveSlots.Add(slotNr + (ycheck * slotsInRow));
                    }
                    for (int xcheck = 0; xcheck < item.sizeX; xcheck++)
                    {
                        if (item.sizeX > 1)
                        {
                            if ((slotNr + 1) % slotsInRow == 0)
                            {
                                if (initialPlacement)
                                {
                                    item.reserveSlots.Clear();
                                }
                                return false;
                            }
                        }
                        if (slotNr + ((slotsInRow * ycheck) + xcheck) >= slotList.Count)
                        {
                            if (initialPlacement)
                            {
                                item.reserveSlots.Clear();
                            }
                            return false;
                        }
                        if (slotList[slotNr + ((slotsInRow * ycheck) + xcheck)].occupied)
                        {
                            if (initialPlacement)
                            {
                                item.reserveSlots.Clear();
                            }
                            return false;
                        }
                        else
                        {
                            if (initialPlacement)
                            {
                                item.reserveSlots.Add(slotNr + (xcheck + (ycheck * slotsInRow)));
                            }
                        }
                    }
                }
            }
            else
            {
                if (initialPlacement)
                {
                    item.reserveSlots.Clear();
                }
                return false;
            }
            if (item.sizeX < 2)
            {
                return true;
            }
        }
        else
        {
            if (initialPlacement)
            {
                item.reserveSlots.Clear();
            }
            return false;
        }
        if (initialPlacement)
        {
            item.SetInitialPos(this);
        }
        return true;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        transform.position = transform.position;
    }

    public void OnDrag(PointerEventData eventData)
    {
        transform.position += new Vector3(eventData.delta.x, eventData.delta.y);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.position = transform.position;
        if (ownerTransform != null)
        {
            if (ownerTransform.GetComponent<Inventory>())
            {
                ownerTransform.GetComponent<Inventory>().savedPos = transform.position;
            }
        }
    }

    public void dropItem(Item item)
    {
        Debug.Log("Dropping item: " + item);
        Item invItem = item;

        if (item.placingInventory != null)
        {
            //find the item in the original inventory / not in visual
            for (int i = 0; i < item.placingInventory.itemList.Count; i++)
            {
                if (item.placingInventory.itemList[i].uniqueID == item.uniqueID)
                {
                    invItem = item.placingInventory.itemList[i];
                    item.placingInventory.itemList.RemoveAt(i);
                }
            }
        }
        else
        {
            this.itemList.Remove(item);
        }

        invItem.gameObject.GetComponent<MeshRenderer>().enabled = true;
        invItem.gameObject.GetComponent<Collider>().enabled = true;

        foreach (int reserve in item.reserveSlots)
        {
            this.slotList[reserve].GetComponent<CanvasGroup>().alpha = 0.4f;
            this.slotList[reserve].occupied = false;
        }

        invItem.reserveSlots.Clear();
        invItem.placingInventory = null;
        invItem.transform.parent = null;

        if (ownerTransform != null)
        {
            ownerTransform.GetComponent<InventoryManager>().removeInventory(item.GetComponent<Inventory>());
            //ownerTransform.GetComponent<PlayerController>().checkCurrentEquipped(item.itemName);
            ownerTransform.GetComponent<InventoryManager>().refreshInventories();
        }

        invItem.transform.position = this.transform.position;
        invItem.gameObject.AddComponent<Rigidbody>();
        if (equipmentManager != null)
        {
            equipmentManager.unEquipItem(invItem);
        }
    }

    public void equipItem(Item item)
    {
        //Get original item;
        if (item.onlyVisual == true)
        {
            foreach (Item s in GetComponent<Inventory>().itemList)
            {
                if (s.uniqueID == item.uniqueID)
                {
                    item = s;
                }
            }
        }
        Transform targetSlot = null;

        if (item.ItemSlotType == ItemSlotTypes.Body)
        {
            targetSlot = IDManager.playerController.transform.GetChild(0).FindChild("ItemSlot_WholeSkelly");
        }

        if (item.ItemSlotType == ItemSlotTypes.Neck)
        {
            targetSlot = IDManager.playerController.transform.GetChild(0).FindChild("ItemSlot_LowerNeck");
        }

        if (item.ItemSlotType == ItemSlotTypes.Head)
        {
            targetSlot = IDManager.playerController.transform.GetChild(0).FindChild("ItemSlot_TopChest");
        }

        if (item.ItemSlotType == ItemSlotTypes.Feet)
        {
            targetSlot = IDManager.playerController.transform.GetChild(0).FindChild("ItemSlot_Feet");
        }

        if (targetSlot != null)
        {
            if (item.GetComponent<Item_Equipment>().EquipmentPiece != null)
            {
                targetSlot.GetComponent<SkinnedMeshRenderer>().sharedMesh = item.GetComponent<Item_Equipment>().EquipmentPiece.GetComponent<SkinnedMeshRenderer>().sharedMesh;
                targetSlot.GetComponent<SkinnedMeshRenderer>().enabled = true;
                Debug.Log("Equipping to: " + targetSlot.ToString() + ", item:" + item.ToString());
            }
            
        }
        IDManager.equipmentManager.EquipItem(item);
    }
}
