﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class EquipmentSlot : MonoBehaviour {

    public Item equippedItem;
    public ItemSlotTypes slotType;

    public void equipItem(Item item)
    {
        equippedItem = item;
        GetComponent<Image>().sprite = item.GetComponent<Image>().sprite;
    }

    public void unEquipItem(Item item)
    {
        equippedItem = null;
        GetComponent<Image>().sprite = null;
    }
}


