﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Item_Equipment : MonoBehaviour
{

    public GameObject EquipmentPiece;
    public Item_ContextMenu contextMenu;
    void Start()
    {
        if (!GetComponent<Item>())
        {
            Debug.Log("Equipment does not represent a real item");
        }

        //GameObject represent = Network.Instantiate(Resources.Load(GetComponent<Item>().itemName + "prefab"), new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0), 100) as GameObject;
    }

    void Update()
    {
        if (GetComponent<Item>().placingInventory != null)
        {
            if (Input.GetMouseButtonDown(1))
            {
                if (Mathf.Abs(GetComponent<RectTransform>().position.x - Input.mousePosition.x) < GetComponent<RectTransform>().rect.size.x / 2 && Mathf.Abs(GetComponent<RectTransform>().position.y - Input.mousePosition.y) < GetComponent<RectTransform>().rect.size.y / 2)
                {
                    openContextMenu();
                }
            }
        }
    }
    public void openContextMenu()
    {
        if (GetComponent<Item_ContextMenu>())
        {
            contextMenu = GetComponent<Item_ContextMenu>();
        }
        else
        {
            gameObject.AddComponent<Item_ContextMenu>();
            contextMenu = GetComponent<Item_ContextMenu>();
        }

        if (contextMenu != null)
        {
            for (int i = 0; i < GetComponent<Item>().placingInventory.openContextMenus.Count - 1; i++)
            {
                GameObject temp = GetComponent<Item>().placingInventory.openContextMenus[i];
                GetComponent<Item>().placingInventory.openContextMenus.RemoveAt(i);
                GameObject.Destroy(temp);
            }

            contextMenu.displayContextMenu(GetComponent<Item>(), Input.mousePosition, transform.parent.parent.parent);
            GetComponent<Item>().placingInventory.openContextMenus.Add(contextMenu.GetComponent<Item_ContextMenu>().contextParent);
        }
    }
}
