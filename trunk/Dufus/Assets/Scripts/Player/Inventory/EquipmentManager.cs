﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Organizes the inventory items to the character screen, i.e. onto the seperate body parts.
/// </summary>
public class EquipmentManager : MonoBehaviour
{

    public List<EquipmentSlot> eSlots = new List<EquipmentSlot>();
    Inventory itemInventory;

    void Start()
    {
        foreach (EquipmentSlot s in GetComponentsInChildren<EquipmentSlot>())
        {
            eSlots.Add(s);
        }
    }

    void Update()
    {
        if (!gameObject.activeSelf)
        {
            return;
        }
        if (Input.GetMouseButtonDown(1))
        {
            for (int i = 0; i < eSlots.Count; i++)
            {
                if (Mathf.Abs(eSlots[i].GetComponent<RectTransform>().position.x - Input.mousePosition.x) < eSlots[i].GetComponent<RectTransform>().rect.size.x / 2 && Mathf.Abs(eSlots[i].GetComponent<RectTransform>().position.y - Input.mousePosition.y) < eSlots[i].GetComponent<RectTransform>().rect.size.y / 2)
                {
                    Debug.Log(eSlots[i].equippedItem);
                    eSlots[i].equippedItem.GetComponent<Item_Equipment>().openContextMenu();
                }
            }
        }
    }

    public void unEquipItem(Item item)
    {
        for (int i = 0; i < eSlots.Count; i++)
        {
            if (eSlots[i].slotType == item.ItemSlotType)
            {
                eSlots[i].unEquipItem(item);
            }
        }
    }

    public void EquipItem(Item item)
    {
        for (int i = 0; i < eSlots.Count; i++)
        {
            if (eSlots[i].slotType == item.ItemSlotType)
            {
                Debug.Log("Equipped: " + item.ToString());
                eSlots[i].equipItem(item);
            }
        }
    }
}
