﻿using UnityEngine;
using System.Collections;

public class IKLegPositioning : MonoBehaviour {

    public CharacterController charController;
    public bool falling;
    public bool leftLeg = false;
    public Animator CharAnimator;
    
	// Use this for initialization
	void Start () 
    {      
        if (charController == null)
        {
            charController = GetComponentInParent<CharacterController>();
            if (charController == null)
            {
                charController = transform.parent.GetComponentInParent<CharacterController>();
                if (charController == null)
                {
                    charController = transform.parent.parent.GetComponentInParent<CharacterController>();
                    if (charController == null)
                    {
                        charController = transform.parent.parent.parent.GetComponentInParent<CharacterController>();
                        if (charController == null)
                        {
                            charController = transform.parent.parent.parent.parent.GetComponentInParent<CharacterController>();
                        }
                    }
                }
            }
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
        float normalizedTime = CharAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime;
        normalizedTime = normalizedTime - Mathf.FloorToInt(normalizedTime);
        if (GetComponent<Animator>())
        {
            if (GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.name == "a_player_Idle_Standing")
            {
                Debug.Log(normalizedTime);
                getDistanceToGround(transform.parent.parent.parent.position);
            }
        }
        
        //Debug.Log("Transform: " + transform.position);
        //Debug.Log("Controller: " + charController.transform.position);
	}
    float getDistanceToGround(Vector3 globalStartPos)
    {
        Vector3 direction = -charController.transform.up;
        Ray ray = new Ray(globalStartPos, direction);
        RaycastHit hit;
        //Debug.DrawRay(ray.origin, ray.direction, Color.red, 1f);
        if (Physics.Raycast(ray, out hit, 15f))
        {
            float[] angles = GetAngles(0.75f, 0.65f, new Vector2((ray.origin - hit.point).magnitude, 0f));
            setAngles(angles[0], angles[1]);
            return (ray.origin - hit.point).magnitude;
        }
        return -1;
    }

    float[] GetAngles(float length1, float length2, Vector2 endPos)
    {
        float angle1 = 0f;
        float angle2 = 0f;

        angle2 = Mathf.Acos((Mathf.Pow(endPos.x, 2) + Mathf.Pow(endPos.x, 2) - Mathf.Pow(length1, 2) - Mathf.Pow(length2, 2)) / (2 * length1 * length2));
        angle1 = (((-(length2 * Mathf.Sin(angle2)) * endPos.x) + (length1 + (length2 * Mathf.Cos(angle2))) * endPos.y)
                / (((length2 * Mathf.Sin(angle2)) * endPos.y) + (length1 + (length2 * Mathf.Cos(angle2))) * endPos.x));

        return new float[] { angle1, angle2 };
    }

    void setAngles(float angle1, float angle2)
    {
        if (leftLeg)
        {
            transform.parent.parent.localEulerAngles = new Vector3(0, -Mathf.Abs(angle1) * 57.2f, 0);
            transform.parent.localEulerAngles = new Vector3(0, Mathf.Abs(angle2) * 57.2f, 0);
        }
        else
        {
            transform.parent.parent.localEulerAngles = new Vector3(0, Mathf.Abs(angle1) * 57.2f, 0);
            transform.parent.localEulerAngles = new Vector3(0, -Mathf.Abs(angle2) * 57.2f, 0);
        }   
    }
}
