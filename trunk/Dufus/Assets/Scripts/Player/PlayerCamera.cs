﻿using UnityEngine;
using System;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class PlayerCamera : NetworkBehaviour {

    public List<GameObject> playerMeshes = new List<GameObject>();
    public GameObject attachCamTo;
    public GameObject camParent;

    private cameraState camState;
    public cameraState CamState
    {
        get
        {
            return camState;
        }
        set
        {
            camState = value;
        }
    }

    public bool freeLook = false;
    public float mouseLookSensitivity;

	void Start () 
    {
        if (isLocalPlayer)
        {
            camParent = GameObject.Instantiate(Resources.Load("Camera")) as GameObject;
            camParent.name = "playerCamera";
            camParent.GetComponent<Camera>().fieldOfView = 65;
            camParent.GetComponent<Camera>().nearClipPlane = 0.2f;
            camParent.transform.position = transform.position + new Vector3(0, 1.63f, 0);

            CamState = cameraState.FirstPerson;
            camParent.transform.SetParent(attachCamTo.transform);
            camParent.transform.localEulerAngles = transform.localEulerAngles;
            camParent.AddComponent<Skybox>();

            GetComponent<PlayerController>().setWeaponParent(camParent.transform);
            GetComponent<PlayerController>().setWeaponHandsPos(new Vector3(0, -0.3f, 0), camParent.transform.rotation);

            foreach (SkinnedMeshRenderer a in GetComponentsInChildren<SkinnedMeshRenderer>())
            {
                playerMeshes.Add(a.gameObject);
            }

            switchCameraState(CamState);
        }
	}


       
    public void cycleStates()
    {
        string[] enumStrings = Enum.GetNames(typeof(cameraState));
        for (int i = 0; i < enumStrings.Length; i++)
        {
            if (CamState.ToString() == enumStrings[i])
            {
                if (i == Enum.GetNames(typeof(cameraState)).Length - 1)
                {
                    switchCameraState((cameraState)Enum.Parse(typeof(cameraState), enumStrings[0].ToString()));
                }
                else
                {
                    switchCameraState((cameraState)Enum.Parse(typeof(cameraState), enumStrings[i + 1].ToString()));
                }
                return;
            }
        }
        
    }
    public void switchCameraState(cameraState cameraState)
    {
        if (cameraState == cameraState.FirstPerson)
        {
            foreach (GameObject c in playerMeshes)
            {
                if (c.tag == "Weapon")
                {
                    c.SetActive(true);
                }
                else
                {
                    c.SetActive(false);
                }
            }
            camParent.GetComponent<Camera>().transform.localPosition = new Vector3(0, 1.63f, 0);
            CamState = cameraState;
            return;
        }

        if (cameraState == cameraState.ThirdPerson)
        {           
            foreach (GameObject c in playerMeshes)
            {
                if (c.tag == "Weapon")
                {
                    c.SetActive(false);
                }
                else
                {
                    c.SetActive(true);
                }
            }           
            camParent.GetComponent<Camera>().transform.localPosition = new Vector3(0, 2, -1);
            CamState = cameraState;
        }
    }

    public void mouseMoveCamera()
    {
        if (!freeLook)
        {
            GetComponent<CharacterController>().transform.localEulerAngles += new Vector3(0, Input.GetAxisRaw("Mouse X") * mouseLookSensitivity, 0);
            camParent.transform.localEulerAngles += new Vector3(-Input.GetAxisRaw("Mouse Y") * mouseLookSensitivity, 0, 0);

            if (camParent.transform.localEulerAngles.x < 272 && camParent.transform.localEulerAngles.x > 88)
            {
                if (camParent.transform.localEulerAngles.x > 180)
                {
                    camParent.transform.localEulerAngles += new Vector3(3f, 0, 0);
                }
                else
                {
                    camParent.transform.localEulerAngles -= new Vector3(3f, 0, 0);
                }
            }
            if (camParent.transform.localEulerAngles.z > 180)
            {
                camParent.transform.localEulerAngles -= new Vector3(3f, 0, 0);
            }
        }
        else if (freeLook)
        {
            camParent.transform.localEulerAngles += new Vector3(-Input.GetAxisRaw("Mouse Y") * mouseLookSensitivity, Input.GetAxisRaw("Mouse X") * mouseLookSensitivity, 0);
        }
    }
    public GameObject ObjInteractionRayCast()
    {
        Ray ray = camParent.GetComponent<Camera>().ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100))
        {
            return (hit.transform.gameObject);
        }
        return null;
    }

}
