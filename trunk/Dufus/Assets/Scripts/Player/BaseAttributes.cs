﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
public class BaseAttributes : NetworkBehaviour
{
    public bool Dead;

    [SyncVar]
    public float Health = 100;
    public string playerName;
    /// <summary>
    /// The blood amount in mL.
    /// </summary>
    public float maxBloodLevel = 5000;
    //[SyncVar]
    public float currentBloodLevel;
    public float bloodLossPerSecond;
    [SyncVar]
    public Factions objFaction;

    public delegate void playerEvent(Object sender);
    public delegate void playerHitEvent(DmgModelHumanParts bodyObj, Vector3 hitPos, Vector3 hitDirection, WoundTypes woundType);
    public event playerEvent bloodChanged;
    public event playerEvent healthChanged;
    public event playerHitEvent newHit;

    public List<BodyPart> bodyParts = new List<BodyPart>();
    public List<ListenBodyPart> bodyPartStatusManager = new List<ListenBodyPart>();
    public List<GameObject> bleedingObjects = new List<GameObject>();
    /// <summary>
    /// - A class II shock category (750-1500ml) would leave “most” dizzy and very weak
    /// a Class III or Class IV shock category (1500ml-2 litres of blood loss) would leave 
    /// “most” with the inability to stand up right
    /// </summary>

    void Start()
    {
        bodyParts.AddRange(GetComponentsInChildren<BodyPart>());

        currentBloodLevel = maxBloodLevel;
        GameObject.Find("_IDManager").GetComponent<IDManager>().playerAttributes = this;

        healthChanged += checkHealth;
        if (bloodChanged != null)
        {
            bloodChanged(null);
        }
        newHit += AddBleedingEffectToPlayer;

    }

    public void setBodyPartListeners()
    {
        for (int i = 0; i < bodyPartStatusManager.Count; i++)
        {
            for (int k = 0; k < bodyParts.Count; k++)
            {
                if (bodyPartStatusManager[i].bodyPartObject == bodyParts[k].bodyPartObject)
                {
                    bodyPartStatusManager[i].listenPart = bodyParts[k];
                }
            }
        }
    }

    void FixedUpdate()
    {
        if (isClient && !isServer)
        {
            if (bloodChanged != null)
            {
                bloodChanged(null);
            }
            currentBloodLevel -= bloodLossPerSecond * Time.fixedDeltaTime;
        }

        if (isServer)
        {
            float curBloodLossPerSec = evaluateBloodLoss();
            //Debug.Log("serverStuff");
            currentBloodLevel -= curBloodLossPerSec * Time.fixedDeltaTime;
            if (curBloodLossPerSec > 0)
            {
                if (bloodChanged != null)
                {
                    bloodChanged(null);
                }
            }
            if (bloodLossPerSecond != curBloodLossPerSec)
            {
                RpcgetBloodStats(curBloodLossPerSec, currentBloodLevel);
            }
        }
    }

    [Server]
    public void changeHealth(float newValue)
    {
        Health = newValue;
        RpcgetCurrentHealth(Health);
        checkHealth(null);
        if (Health <= 0)
        {

        }
    }

    [Server]
    public void substractHealth(float substractValue)
    {
        changeHealth(Health - substractValue);
        checkHealth(null);
    }


    public void changeName(string name)
    {
        playerName = name;
        gameObject.name = "player: " + name;
    }

    [Server]
    public void dmgModelHit(BodyPart bodyPart, float penetration, Vector3 hitPoint, Vector3 direction)
    {
        WoundTypes woundType = new WoundTypes();
        if (penetration > 0.05f && penetration < 0.1f)
        {
            woundType = WoundTypes.ShallowWound;
        }
        if (penetration >= 0.1f)
        {
            woundType = WoundTypes.DeepWound;
        }

        //should be unique names/hitboxes.

        bodyPart.AddWound(woundType);
        newHit(bodyPart.bodyPartObject, hitPoint, direction, woundType);
        RpccallHitEvent(bodyPart.bodyPartObject, hitPoint, direction, woundType);

    }
    [ClientRpc]
    public void RpccallHitEvent(DmgModelHumanParts bodyPart, Vector3 point, Vector3 hitDirection, WoundTypes woundType)
    {
        if (newHit != null)
        {
            //new hit event calls to add a bleeding effect to the player.
            Debug.Log("rpcHitEVent");
            newHit(bodyPart, point, hitDirection, woundType);
        }
    }

    [Client]
    public void AddBleedingEffectToPlayer(DmgModelHumanParts senderObj, Vector3 closestPoint, Vector3 direction, WoundTypes woundType)
    {

        BodyPart bodyPart = null;
        for (int i = 0; i < bodyParts.Count; i++)
        {
            if (senderObj == bodyParts[i].bodyPartObject)
            {
                bodyPart = bodyParts[i];
                bodyParts[i].AddWound(woundType);
                //return;
            }
        }
        if (bodyPart != null)
        {
            //Debug.Log(bodyPart.name);
            GameObject bleedingEffect = Resources.Load("BleedingEffectSmall") as GameObject;

            if (woundType == WoundTypes.DeepWound)
            {
                bleedingEffect = Resources.Load("BleedingEffectLarge") as GameObject;
            }
            if (woundType == WoundTypes.ShallowWound)
            {
                bleedingEffect = Resources.Load("BleedingEffectMedium") as GameObject;
            }

            if (bleedingEffect != null)
            {
                GameObject bleeding = GameObject.Instantiate(bleedingEffect, closestPoint, Quaternion.identity) as GameObject;
                bleeding.transform.parent = bodyPart.transform;
                bleeding.name = "BleedObject";
                bleedingObjects.Add(bleeding);
                bleeding.transform.forward = -direction;

                GameObject wound = GameObject.Instantiate(Resources.Load("gunShotWound"), closestPoint, Quaternion.identity) as GameObject;
                wound.transform.parent = bodyPart.transform;
                wound.name = "WoundObject";
                wound.transform.LookAt(GetComponent<Collider>().bounds.center);
                wound.transform.eulerAngles += new Vector3(0, 0, 180);
                //Debug.Log(bodyPart.ToString() + ", " + bodyPart.ToString());
            }
        }
    }

    [Server]
    public float evaluateBloodLoss()
    {
        float BloodLossPerSec = 0;
        foreach (BodyPart bp in bodyParts)
        {
            foreach (WoundTypes wt in bp.Wounds)
            {
                BloodLossPerSec += 20;
            }
        }

        checkHealth(null);
        return BloodLossPerSec;
    }

    [ClientRpc]
    public void RpcgetCurrentHealth(float health)
    {
        Health = health;
        if (healthChanged != null)
        {
            healthChanged(null);
        }
    }

    [ClientRpc]
    public void RpcgetBloodStats(float bloodLoss, float curBloodLevel)
    {
        currentBloodLevel = curBloodLevel;
        bloodLossPerSecond = bloodLoss;
    }

    [Server]
    private void checkHealth(Object sender)
    {
        //if (Health <= 0)
        //{
        //Something like addcomponent<ragdollScript>();
        //ragdollScript.dead = true;
        //    GameObject ragdoll;
        //    ragdoll = GameObject.Instantiate((GameObject)Resources.Load("AIRagdoll"), transform.position, transform.rotation) as GameObject;
        //    ragdoll.AddComponent<NetworkIdentity>();
        //    NetworkServer.Spawn(ragdoll);
        //    NetworkServer.Destroy(gameObject);

        //    return;
        //}
        //if (currentBloodLevel < 3000)
        //{
        //    GameObject ragdoll;
        //    ragdoll = GameObject.Instantiate((GameObject)Resources.Load("AIRagdoll"), transform.position, transform.rotation) as GameObject;
        //    ragdoll.AddComponent<NetworkIdentity>();
        //    NetworkServer.Spawn(ragdoll);
        //    NetworkServer.Destroy(gameObject);

        //if (gameObject.GetComponent<NetworkIdentity>().isClient)
        //{
        //    ClientScene.RemovePlayer(gameObject.GetComponent<NetworkTransform>().playerControllerId);
        //    ClientScene.AddPlayer(gameObject.GetComponent<NetworkTransform>().playerControllerId);
        //}
        return;
    }
}

