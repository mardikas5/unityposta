﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;
public class DayNightCycle : NetworkBehaviour
{
    public float cycleLength = 20f;
    public float intervalSize = 2f;
    public float PercentofDay = 0.75f;
    public float maxIntens = 0;
    public float minIntens = 0;
    public float currentIntens = 0;

    public bool doSync = false;
    public float transitionPeriod = 10f;
    public float currentColor;
    public float offSet = 0f;
    public float sunHeight;

    public float maxAngle = 60f;
    public float minAngle = -60f;

    public string timeOfDay;

    private float sunTimer = 0f;
    private float interval = 0.1f;
    // Use this for initialization
    void Start()
    {
        interval = intervalSize / cycleLength;
        transform.eulerAngles = new Vector3(0, 180, 0);

       // GetComponent<NetworkView>().RPC("syncDayTimeRequest", RPCMode.Server);
        //maxRisingAngle = 360 * (0.75f / 2);
    }

    // Update is called once per frame
    void Update()
    {
        if (sunTimer < 0)
        {
            if (doSync)
            {
                CmdsyncDayTimeRequest();
            }
            transform.eulerAngles -= new Vector3(0, intervalSize, 0);
            transform.eulerAngles = new Vector3(Mathf.Sin((transform.eulerAngles.y / 180) * Mathf.PI) * maxAngle, transform.eulerAngles.y, 0);

            double minutes = Math.Round(1440 - (1440 * (transform.eulerAngles.y / 360)));
            double hours = Mathf.FloorToInt(float.Parse(minutes.ToString()) / 60);
            
            hours = hours - 3;
            if (hours < 0)
            {
                hours = hours + 24;
            }

            timeOfDay = hours.ToString() + ":" + Math.Round(minutes % 60, 2);

            if (transform.eulerAngles.x > 90)
            {
                sunHeight = -(360 - transform.eulerAngles.x);
            }
            else
            {
                sunHeight = transform.eulerAngles.x;
            }

            GetComponent<Light>().intensity = Mathf.Sin((transform.eulerAngles.y / 180) * Mathf.PI) * Mathf.Abs(sunHeight);
            GetComponent<Light>().shadowStrength = Mathf.Abs(sunHeight) * 0.018f;
            sunTimer = cycleLength;
            
            if (sunHeight < transitionPeriod)
            {
                GetComponent<Light>().intensity = 0.1f * ((transitionPeriod) + sunHeight);
                
                float totalHeight = transitionPeriod * 2;
                float color = 254 + ((-transitionPeriod + sunHeight) * (255 / totalHeight));
                currentColor = color;

                RenderSettings.fogColor = Color.white * (1 / (255 - color));
                if (RenderSettings.fogColor.b > 255)
                {
                    RenderSettings.fogColor = Color.white;
                }
                RenderSettings.fogDensity = 0.001f;
            }

            if (GetComponent<Light>().intensity > maxIntens)
            {
                GetComponent<Light>().intensity = maxIntens;
            }

            if (GetComponent<Light>().intensity < 0.001)
            {
                GetComponent<Light>().intensity = 0.001f;
            }
            //float yellowGradient = (1 / (-transitionPeriod + Mathf.Abs(sunHeight)));
            //if (yellowGradient > 1)
            //{
            //    yellowGradient = 1;
            //}
            //GetComponent<Light>().color = Color.white - (Color.blue * yellowGradient);

            
            RenderSettings.ambientIntensity = RenderSettings.reflectionIntensity = GetComponent<Light>().intensity;
            currentIntens = GetComponent<Light>().intensity;
        }
        sunTimer -= Time.fixedDeltaTime;
    }

    [Command]
    void CmdsyncDayTimeRequest()
    {
        RpcsyncDayTimeAnswer(transform.localEulerAngles);
    }
    [ClientRpc]
    void RpcsyncDayTimeAnswer(Vector3 SunAngles)
    {
        transform.eulerAngles = SunAngles;
    }
}

