﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;

public class BaseWeapon : NetworkBehaviour
{
    public string Name = "DefaultBase";

    private float FireRate = 1f;
    private float previousShotTime = 0f;
    private int recoilStacks = 0; //the amount of recoil stacks affects the next recoil amount

    public int wepType;
    public int ClipSize = 15;

    public bool WeaponArmed = false;
    public float fireRateInterval = 1f;
    public float effectiveDistance = 200f;
    [SyncVar]
    public Quaternion syncQuart;

    public float stackingRecoil = 10f;
    public int maxRecoilStacks = 4;
    public float recoilTimer = 0.3f;
    public float recoilMaxX = 3f;
    public float recoilMaxY = 3f;

    [SyncVar]
    public NetworkInstanceId owner;
    public Animator WeaponAnimator;

    private GameObject Current;
    private GameObject ChangeTo;
    GameObject myBulletPrefab;

    /// <summary>
    /// Accuracy 100 is perfect 0 is non-existent.
    /// </summary>
    public float accuracy;
    public Transform gunBarrel = null;
    public delegate void shotEventHandler(object sender);
    public event shotEventHandler shotEvent;

    void Awake()
    {
        FireRate = fireRateInterval;
    }

    void Start()
    {
        fireRateInterval = FireRate;
        if (isServer)
        {
            owner = GetComponent<NetworkIdentity>().netId;
            if (WeaponAnimator != null)
            {
                WeaponArmed = true;
                WeaponAnimator.SetBool("combatIdle", true);
            }
        }
    }

    [Command]
    void CmdSendAngles(Quaternion rot)
    {
        syncQuart = rot;
    }
    public virtual void secondaryAction()
    {
        WeaponAnimator.SetBool("secondary", true);
    }

    public void changeWeaponArmState(bool armed)
    {
        if (armed)
        {
            WeaponAnimator.SetBool("combatIdle", true);
            WeaponArmed = true;
        }
        else
        {
            WeaponAnimator.SetBool("combatIdle", false);
            WeaponArmed = false;
        }
    }


    public void CmdfireWeapon(Quaternion weaponRotation)
    {
        if (WeaponAnimator != null)
        {
            if (WeaponAnimator.GetBool("readyToShoot") == false || FireRate > Time.time)
            {
                return;
            }
        }
        if (FireRate < Time.time)
        {
            if (WeaponAnimator != null)
            {

                int animNr = Random.Range(0, WeaponAnimator.GetInteger("fireAnims"));
                WeaponAnimator.GetComponent<Animator>().SetBool("isFiring", true);
                WeaponAnimator.GetComponent<Animator>().SetInteger("fireAnim", animNr);
                if (owner != null)
                {
                    transform.GetComponent<Animator>().SetInteger("fireAnim", animNr);
                }
            }
            if (shotEvent != null)
            {
                shotEvent(this);
            }
            if (myBulletPrefab == null)
            {
                myBulletPrefab = Resources.Load("BulletPrefab") as GameObject;
            }
            //if (isLocalPlayer)
            //{
            
            syncQuart = weaponRotation;
            CmdSendAngles(syncQuart);
            
            CmdCreateBullet();

            FireRate = Time.time + fireRateInterval;

            float randomRecoilX = Random.Range(-recoilMaxX, recoilMaxX);
            float randomRecoilY = Random.Range(0, recoilMaxY);

            if (Time.time - recoilTimer < previousShotTime)
            {
                if (recoilStacks < 3)
                {
                    recoilStacks++;
                }

                //recoil += new Vector2(randomRecoilY + (recoilStacks * 0.5f), randomRecoilX * (recoilStacks * 0.5f)) * (Time.time - previousShotTime) * stackingRecoil;

            }
            else
            {
                //recoil += new Vector2(randomRecoilX, randomRecoilY);
                recoilStacks = 0;
            }
        }
        previousShotTime = Time.time;
    }

    [Command]
    public void CmdCreateBullet()
    {
        if (myBulletPrefab == null)
        {
            myBulletPrefab = Resources.Load("BulletPrefab") as GameObject;
        }
        
        GameObject myBullet = (GameObject)GameObject.Instantiate(myBulletPrefab, transform.position + new Vector3(0, 1.63f, 0), syncQuart);
        myBullet.GetComponent<Bullet>().owner = GetComponent<NetworkIdentity>().netId;
        NetworkServer.Spawn(myBullet);
        
    }

    public void changeWeapon(GameObject current, GameObject changeTo)
    {
        WeaponAnimator.SetBool("combatIdle", false);
        WeaponAnimator.SetBool("destroy", true);
        WeaponArmed = false;

        Current = current;
        ChangeTo = changeTo;
    }
    public void setPreviousPos()
    {
        ChangeTo.transform.position = Current.transform.position;
        ChangeTo.transform.rotation = Current.transform.rotation;
        ChangeTo.transform.parent = Current.transform.parent;
    }
}
