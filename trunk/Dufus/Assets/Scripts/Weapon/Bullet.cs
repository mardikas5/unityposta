﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Bullet : NetworkBehaviour
{
    public float timer = 0;
    private float updateTimer = 0;
    public GameObject colObj = null;
    [SyncVar]
    public NetworkInstanceId owner;

    Vector3 startpos;
    Vector3 direction;

    float speed = 360f;
    float distance = 0;

    void Start()
    {
        direction = transform.forward;
        timer = Time.time;

    }

    void Update()
    {

    }

    void FixedUpdate()
    {
        if (gameObject != null)
        {
            if (isClient && !isServer)
            {
                doLocalRayCasts();
            }
            if (isServer)
            {
                doServerRayCasts();
            }
        }
        else
        {
            return;
        }
        if (GetComponent<NetworkTransform>())
        {
            GameObject.Destroy(GetComponent<NetworkTransform>());
        }
    }
    void LateUpdate()
    {
        //if (GetComponent<NetworkTransform>())
        //{
        //    GameObject.Destroy(GetComponent<NetworkTransform>());
        //}
    }
    [Server]
    public void doServerRayCasts()
    {
        float distancePerFrame = Time.fixedDeltaTime * speed;
        updateTimer += Time.fixedDeltaTime;
        distance += distancePerFrame;
        RaycastHit hit;
        for (int i = 0; i < 5; i++)
        {
            if (Physics.Raycast(transform.position, direction, out hit, distancePerFrame))
            {
                if (hit.transform.GetComponentInParent<BaseWeapon>())
                {
                    if (hit.transform.GetComponentInParent<BaseWeapon>().owner != owner)
                    {
                        checkForDmg(hit);
                        Debug.Log("Hit non-playeR");
                        NetworkServer.Destroy(gameObject);
                        return;
                    }
                    else
                    {
                        transform.position += direction.normalized * 0.1f;
                    }
                }
                else
                {
                    NetworkServer.Destroy(gameObject);
                    return;
                }
            }
        }
        transform.position += direction * distancePerFrame;
        if (timer + 3 < Time.time)
        {
            NetworkServer.Destroy(gameObject);
        }
    }
    public void doLocalRayCasts()
    {
        if (gameObject == null)
        {
            return;
        }
        float distancePerFrame = Time.deltaTime * speed;
        updateTimer += Time.deltaTime;
        distance += distancePerFrame;
        RaycastHit hit;

        for (int i = 0; i < 5; i++)
        {
            if (Physics.Raycast(transform.position, direction, out hit, distancePerFrame))
            {
                if (hit.transform.GetComponentInParent<BaseAttributes>())
                {
                    if (hit.transform.GetComponentInParent<BaseWeapon>().owner != owner)
                    {
                        transform.position += direction * distancePerFrame;
                        return;
                    }
                    else
                    {
                        GameObject.Destroy(gameObject);
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
        }
        transform.position += direction * distancePerFrame;
        if (timer + 3 < Time.time)
        {
            NetworkServer.Destroy(gameObject);
        }
    }
    [Server]
    public void checkForDmg(RaycastHit hit)
    {
        if (hit.transform.GetComponentInParent<BaseAttributes>())
        {
            BaseAttributes temp = hit.transform.GetComponentInParent<BaseAttributes>();

            float totalPenetration = 0;
            do
            {
                totalPenetration += 0.01f;
            }
            while (hit.collider.bounds.Contains(hit.point + (direction.normalized * totalPenetration)));

            if (hit.transform.GetComponent<BodyPart>())
            {
                temp.dmgModelHit(hit.transform.GetComponent<BodyPart>(), totalPenetration, hit.point, direction);
            }
            else
            {
                if (temp != null)
                {
                    //temp.substractHealth(10);
                }
            }
            NetworkServer.Destroy(gameObject);
        }
    }

    public void updateShotPos(Vector3 pos, Vector3 forward)
    {
        gameObject.transform.position = pos;
        gameObject.transform.forward = forward;
        updateTimer = -updateTimer;
    }
}
