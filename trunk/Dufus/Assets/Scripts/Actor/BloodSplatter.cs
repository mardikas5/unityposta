﻿using UnityEngine;
using System.Collections;

public class BloodSplatter : MonoBehaviour
{
    public GameObject VisibleMeshObject;
    public Shader Shader;

    protected Camera UVCamera;
    public RenderTexture UVRTT;
    protected int oldLayer;
    protected Texture2D detailMask;

    protected static readonly int DetailMaskSize = 128;
    protected static readonly int RTTSize = 128;
    protected static readonly float MaxZDifference = 0.2f;
    protected static float[,] SplatMask;

	void Start()
	{
        if( SplatMask == null )
        {
            SplatMask = new float[RTTSize, RTTSize];

            Vector2 center = new Vector2( RTTSize, RTTSize ) / 2.0f;

            for( int y = 0; y < RTTSize; ++y )
            {
                for( int x = 0; x < RTTSize; ++x )
                {
                    SplatMask[x, y] = Mathf.Clamp01( 1.0f - ( Vector2.Distance( center, new Vector2( x, y ) ) / ( RTTSize / 4.0f ) ) );
                }
            }
        }


        detailMask = new Texture2D( DetailMaskSize, DetailMaskSize, TextureFormat.Alpha8, false, false );        
        Color32[] colors = detailMask.GetPixels32();
        for( int y = 0; y < DetailMaskSize; ++y )
        { 
            for( int x = 0; x < DetailMaskSize; ++x )
            {
                colors[x + y * DetailMaskSize] = new Color32( 0, 0, 0, 0 );
            }
        }
        detailMask.SetPixels32( colors );
        detailMask.Apply();
        
        UVRTT = new RenderTexture( RTTSize, RTTSize, 1 );
        UVCamera = GetComponentInChildren<Camera>();
        UVCamera.targetTexture = UVRTT;
        UVCamera.cullingMask = 1 << LayerMask.NameToLayer( "BloodCamera" );
        UVCamera.enabled = false;

        Material targetMaterial = null;
        Renderer[] renderers = VisibleMeshObject.GetComponentsInChildren<Renderer>();
        foreach( Renderer renderer in renderers )
        {
            if( targetMaterial == null ) 
                targetMaterial = renderer.sharedMaterial;
            else if( targetMaterial != renderer.sharedMaterial )
            {
                Debug.LogError( "Script only supports 1 material, fix this." );
                return;
            }
        }

        targetMaterial.SetTexture( "_DetailMask", detailMask );
	}
    
    protected void SetObjectLayer( GameObject current, int layer )
    {
        if( current == null ) return;

        current.layer = layer;
        foreach( Transform child in current.transform )
        {
            SetObjectLayer( child.gameObject, layer );
        }
    }
    
    public void TakeHit( RaycastHit info )
    {
        oldLayer = VisibleMeshObject.layer;
        SetObjectLayer( VisibleMeshObject, LayerMask.NameToLayer( "BloodCamera" ) );
        
        UVCamera.transform.position = info.point + info.normal * 10.0f;
        UVCamera.transform.LookAt( info.point, Vector3.up );
        UVCamera.RenderWithShader( Shader, "" );

        ApplySplatMask();

        SetObjectLayer( VisibleMeshObject, oldLayer );
    }

    public void ApplySplatMask()
    {
        RenderTexture.active = UVRTT;
        Texture2D tmp = new Texture2D( RTTSize, RTTSize, TextureFormat.RGB24, false );
        tmp.ReadPixels( new Rect( 0, 0, RTTSize, RTTSize ), 0, 0, false );        
        RenderTexture.active = null;

        Color32[] colors = tmp.GetPixels32();
        Color32[] detailColors = detailMask.GetPixels32();

        int detailWidth = detailMask.width;
        int detailHeight = detailMask.height;

        int HalfRTTSize = RTTSize / 2;

        float centerZ = colors[HalfRTTSize + HalfRTTSize * RTTSize].b;

        for( int y = 0; y < RTTSize; ++y )
        {
            for( int x = 0; x < RTTSize; ++x )
            {
                Color32 c = colors[x + y * RTTSize];
                Vector2 uv = new Vector2( c.r / 255.0f, c.g / 255.0f );

                int uvx = (int)( uv.x * detailWidth - 1 );
                int uvy = (int)( uv.y * detailHeight - 1 );

                if (uv.sqrMagnitude == 0.0f) continue;

                int a = detailColors[uvx + uvy * detailWidth].a;
                a += (int)( 6.0f * SplatMask[x, y] );
                a = Mathf.Clamp( a, 0, 255 );
                detailColors[uvx + uvy * detailWidth] = new Color32( 0, 0, 0, (byte)a );
            }
        }

        detailMask.SetPixels32( detailColors );
        detailMask.Apply();
    }
}
