﻿using UnityEngine;
using System.Collections;

public class BaseActor : MonoBehaviour {


    private string Name;
    private float Health;


    public BaseActor(string name, float health)
    {
        this.Name = name;
        this.Health = health;
    }

    public float health
    {
        get { return this.Health; }
        set { health = value; }
    }


}
