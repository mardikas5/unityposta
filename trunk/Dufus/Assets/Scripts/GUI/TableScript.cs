﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TableScript : MonoBehaviour {

    public RectTransform serverTransform;
    public Vector3 TransformPos;

	void Start () 
    {
        
        if (serverTransform == null)
        {
            serverTransform = GameObject.Find("ServerTable").GetComponent<RectTransform>();
            //Debug.Log(serverTransform.localPosition + ", " + Screen.width);
        }
        if (TransformPos == null || TransformPos == new Vector3(0,0,0))
        {
            TransformPos = new Vector3(Screen.width - (serverTransform.sizeDelta.x + Screen.width * 0.6f), 0, 0);
        }
	}
	
	
	void Update () 
    {
        serverTransform.localPosition = Vector3.Lerp(serverTransform.localPosition, TransformPos, 0.2f);
        serverTransform.anchoredPosition = new Vector2(0, 0);
	}
}
