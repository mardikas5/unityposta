﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Scoreboard : MonoBehaviour {

    GameObject template;
    GameObject scorePanel;
	// Use this for initialization
	void Start () 
    {
        template = GameObject.Find("Sample");
        scorePanel = GameObject.Find("ScorePanel");
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            createRow();
        }
	}
    void createRow()
    {
        template = GameObject.Instantiate(template) as GameObject;
        template.GetComponent<RectTransform>().localPosition = scorePanel.transform.position;

        template.GetComponent<RectTransform>().localScale.Set(1, 1, 1);
        template.GetComponent<RectTransform>().sizeDelta = new Vector2(scorePanel.GetComponent<RectTransform>().rect.width, 100);

        template.GetComponent<Text>().text = "test";
        template.transform.SetParent(scorePanel.transform);
    }
}
