﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GUIPos : MonoBehaviour {

    public GameObject fixedNameBox;
    public GameObject fixedStartButton;

    public GameObject fixedNewServer;
    public GameObject fixedRefreshServer;

    public GameObject fixedExitBox;

    void Awake()
    {
        //fixedNameBox.GetComponent<RectTransform>().localPosition = new Vector3(Screen.width * 2, 0, 0);
        //fixedStartButton.GetComponent<RectTransform>().localPosition = new Vector3(Screen.width * 2, 0, 0);
    }

	void Start () 
    {
        fixedNameBox = GameObject.Find("tBoxName");
        fixedStartButton = GameObject.Find("btnDone");

        fixedRefreshServer = GameObject.Find("btnRefreshServers");
        fixedNewServer = GameObject.Find("btnNewServer");

        fixedExitBox = GameObject.Find("btnExit");
	}
	
	// Update is called once per frame
	void Update () 
    {
        //updateButtonPositions();
        //doInputs();
	}

    void updateButtonPositions()
    {
		Vector3 fixedPos = new Vector3 (0, 0, 0);


        //fixedNameBox.GetComponent<RectTransform>().localPosition = Vector3.Lerp(fixedNameBox.GetComponent<RectTransform>().localPosition, fixedPos, 0.05f);
        //fixedStartButton.GetComponent<RectTransform>().localPosition = Vector3.Lerp(fixedStartButton.GetComponent<RectTransform>().localPosition, fixedPos + new Vector3(0, -50, 0), 0.05f);

        //fixedRefreshServer.GetComponent<RectTransform>().localPosition = Vector3.Lerp(fixedRefreshServer.GetComponent<RectTransform>().localPosition, fixedPos, 0.05f);
        //fixedNewServer.GetComponent<RectTransform>().localPosition = Vector3.Lerp(fixedNewServer.GetComponent<RectTransform>().localPosition, fixedPos + new Vector3(-2000,0, 0), 0.05f);

        //fixedExitBox.GetComponent<RectTransform>().localPosition = Vector3.Lerp(fixedExitBox.GetComponent<RectTransform>().localPosition, fixedPos + new Vector3(0, -150, 0), 0.05f);
    }

    void doInputs()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BackTransition();
            Transition();
        }
    }

    public void BackTransition()
    {
        for (int i = transform.childCount - 1; i > 0; i--)
        {
            if (transform.GetChild(i).GetComponent<Canvas>().enabled == true)
            {
                transform.GetChild(i - 1).GetComponent<Canvas>().enabled = true;
                transform.GetChild(i).GetComponent<Canvas>().enabled = false;


                for (int k = transform.GetChild(i - 1).childCount - 1; k >= 0; k--)
                {
                    if (transform.GetChild(i - 1).GetChild(k).GetComponent<RectTransform>())
                    {
                        transform.GetChild(i - 1).GetChild(k).GetComponent<RectTransform>().localPosition = new Vector3(Screen.width * 2, 0, 0);
                    }
                }

                i = -1;
            }
        }
    }
    public void Transition()
    {
        for (int i = transform.childCount - 1; i > 0; i--)
        {
            if (transform.GetChild(i).GetComponent<Canvas>().enabled == true)
            {
                for (int k = transform.GetChild(i).childCount - 1; k >= 0; k--)
                {
                    if (transform.GetChild(i).GetChild(k).GetComponent<RectTransform>())
                    {
                        transform.GetChild(i).GetChild(k).GetComponent<RectTransform>().localPosition = new Vector3(-400, Screen.height * 1, 0);
                    }
                }

                i = -1;
            }
        }
    }

    public void deepClickAnim(Button tButton)
    {
        tButton.transform.localPosition += new Vector3(-50, 0, 100);
    }

    public void changePos(GameObject obj, Vector3 newPos, float speed)
    {
        obj.GetComponent<RectTransform>().localPosition = Vector3.Lerp(obj.GetComponent<RectTransform>().localPosition, newPos, speed);
    }
}
