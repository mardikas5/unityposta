﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class ListenBodyPart : MonoBehaviour, IPointerEnterHandler
{

    public BodyPart listenPart = null;
    public DmgModelHumanParts bodyPartObject;

    public playerManagerGUI playerManagerGui;

    void Start()
    {
        gameObject.AddComponent<EventTrigger>();
        GetComponent<Button>().onClick.RemoveAllListeners();
        GetComponent<Button>().onClick.AddListener(() => { playerManagerGui.createStatusPanel(listenPart, bodyPartObject, listenPart.statusToString()); }); 
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        playerManagerGui.createStatusPanel(listenPart, bodyPartObject, listenPart.statusToString());
    }
}
