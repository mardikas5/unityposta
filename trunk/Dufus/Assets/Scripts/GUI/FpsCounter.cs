﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FpsCounter : MonoBehaviour
{

    public Text targetText;
    // Use this for initialization
    void Start()
    {
        if (targetText == null)
        {
            targetText = GameObject.Find("FpsCounter").GetComponent<Text>();
        }
    }
    float timer = 0;
    float refreshRate = 0.25f;

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Q))
        {
            GameObject.Find("Debug").GetComponent<Canvas>().enabled = !GameObject.Find("Debug").GetComponent<Canvas>().enabled;
        }
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            targetText.text = "FPS: " + Mathf.Round(1 / Time.deltaTime);
            timer = refreshRate;
        }
    }
}
