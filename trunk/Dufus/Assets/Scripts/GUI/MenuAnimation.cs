﻿using UnityEngine;
using System.Collections;

public class MenuAnimation : MonoBehaviour {

    public float animationSpeed = 1f;
    bool goingUp = false;

	void Start () 
    {
	
	}
	
	void Update () 
    {
        if (goingUp)
        {
            if (this.transform.localPosition.y > 90)
            {
                this.transform.localPosition -= new Vector3(0, animationSpeed, 0);
            }
            else
            {
                goingUp = false;
            }
        }
        else
        {
            if (this.transform.localPosition.y < 120)
            {
                this.transform.localPosition += new Vector3(0, animationSpeed, 0);
            }
            else
            {
                goingUp = true;
            }
        }
        if (transform.localPosition.x != 0)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(0, transform.localPosition.y, 0), 0.15f);
        }
	}
}
