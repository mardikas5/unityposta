﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class HoverNames : MonoBehaviour
{
    public List<GameObject> followList = new List<GameObject>();
    private List<GameObject> textObjs = new List<GameObject>();
    private List<string> Labels = new List<string>();
    public Camera playerCam;
    private GameObject labelParent;

    void Start()
    {
        labelParent = GameObject.Find("PlayerGUILabels");
    }

    void Update()
    {
        checkHoverLabels();      
        if (playerCam != null)
        {
            labelParent.transform.parent.GetComponent<Canvas>().worldCamera = playerCam;
        }
    }
    private void checkHoverLabels()
    {
        for (int i = 0; i < followList.Count; i++)
        {
            if (followList[i] == null)
            {
                followList[i] = GameObject.Find("_Networking").GetComponent<NetworkScript>().players[i].gameObject;
                if (followList[i] == null)
                {
                    followList.RemoveAt(i);
                }
            }
            else
            {
                Vector3 point = playerCam.WorldToScreenPoint(followList[i].transform.position + new Vector3(0, 1.8f, 0));
                if (point.z > 0)
                {
                    textObjs[i].GetComponent<Text>().enabled = true;                    
                    textObjs[i].GetComponent<Text>().text = Labels[i] + " " + followList[i].GetComponent<BaseAttributes>().Health;

                    float x = point.x;
                    float y = point.y;

                    Vector2 rect = textObjs[i].GetComponent<Text>().rectTransform.sizeDelta;
                    textObjs[i].transform.localPosition = new Vector3(((point.x / 1.2f) - (Screen.width / 2)) + rect.x, (point.y - (Screen.height / 2)) - rect.y / 2, point.z);

                    textObjs[i].GetComponent<Text>().fontSize = Mathf.CeilToInt(50 - point.z * 0.5f);
                    if (textObjs[i].GetComponent<Text>().fontSize <= 0)
                    {
                        textObjs[i].GetComponent<Text>().fontSize = 1;
                    }
                }
                else
                {
                    textObjs[i].GetComponent<Text>().enabled = false;
                }
            }
        }
    }
    /// <summary>
    /// Adds the current object to items to list with a hovering label.
    /// </summary>
    /// <param name="Obj"></param>
    /// <param name="Label"></param>
    public void addToList(GameObject Obj, string Label)
    {
        if (!followList.Contains(Obj))
        {
            followList.Add(Obj);
            Labels.Add(Label);

            GameObject tempObj = Instantiate(Resources.Load("basePlayerLabel")) as GameObject;
            tempObj.GetComponent<Text>().text = Label;

            if (labelParent == null)
            {
                labelParent = GameObject.Find("PlayerGUILabels");
            }

            tempObj.transform.SetParent(labelParent.transform);
            tempObj.transform.localEulerAngles = new Vector3(0, 0, 0);
            tempObj.transform.localScale = new Vector3(1, 1, 1);
            textObjs.Add(tempObj);
            Debug.Log(Obj + " " + Label);
        }
    }
    public void removeFromList()
    {

    }
}
