﻿using UnityEngine;
using System.Collections;

public class TriggerScript : MonoBehaviour
{
    public delegate void triggerHandler(GameObject sender);
    public event triggerHandler triggerEnter;
    void OnTriggerEnter(Collider other)
    {
        if (other.transform.GetComponent<BaseAttributes>())
        {
            triggerEnter(other.gameObject);
        }
    }
}
