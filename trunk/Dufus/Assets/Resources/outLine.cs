﻿using UnityEngine;
using System.Collections;

public class outLine : MonoBehaviour {

    public Shader ShaderToUse;
    private Shader PreviousShader;

    public float shaderTimer;

	void Awake () 
    {

        shaderTimer = Time.time + 0.2f;
        if (ShaderToUse == null)
        {
            ShaderToUse = Shader.Find("Outlined/Silhouetted Diffuse");
        }
		PreviousShader = gameObject.transform.GetComponent<Renderer>().material.shader;
	}

	
	void Update () 
    {
        if (shaderTimer > Time.time)
        {
			gameObject.transform.GetComponent<Renderer>().material.shader = ShaderToUse;
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                if (gameObject.transform.GetChild(i).GetComponent<Renderer>())
                {
					gameObject.transform.GetChild(i).GetComponent<Renderer>().material.shader = ShaderToUse;
                }
            }
        }
        else
        {
			gameObject.transform.GetComponent<Renderer>().material.shader = PreviousShader;
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                if (gameObject.transform.GetChild(i).GetComponent<Renderer>())
                {
					gameObject.transform.GetChild(i).GetComponent<Renderer>().material.shader = PreviousShader;
                }
            }
            Destroy(GetComponent<outLine>());
        }
	}
    public void AddTime(float Time)
    {
        shaderTimer += Time;
    }
}
