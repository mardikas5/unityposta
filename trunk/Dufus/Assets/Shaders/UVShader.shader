﻿Shader "Custom/UVShader" {
	Properties {
	}
	SubShader {
		Pass {
			CGPROGRAM
		
			#pragma vertex vert
            #pragma fragment frag
            #pragma target 3.0

            struct vertexInput 
			{
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };

            struct fragmentInput
			{
                float4 position : SV_POSITION;
                float2 texcoord0 : TEXCOORD0;
				float4 pos : TEXCOORD1;
            };

            fragmentInput vert( vertexInput i )
			{
                fragmentInput o;

				float4 pos = mul( UNITY_MATRIX_MVP, i.vertex );

                o.position = pos;
                o.texcoord0 = i.texcoord0;
				o.pos = pos;
                return o;
            }

            float4 frag( fragmentInput i ) : SV_Target 
			{
                return float4( i.texcoord0, i.pos.z / i.pos.w, 1.0 );
            }


			ENDCG
		}
	} 
	FallBack "Diffuse"
}
